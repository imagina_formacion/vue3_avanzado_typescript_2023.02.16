// define message and style
const styles = ['background: green','color: white', 'display: block'].join(';');
const message = ` >> Service Worker file, sw.js, registered `;

// using the styles and message variable
console.info('%c%s', styles, message);

// install our SW
self.addEventListener('install', (event) => {
  console.info('%c%s', styles, '[SW] Install ...');
})
// activate el SW
self.addEventListener('activate', (event) => {
  console.info('%c%s', styles, '[SW] Activate ...');
})

// allows intercepting requests coming out of our browser
self.addEventListener('fetch', (event) => {
  console.info('%c%s', styles, '[SW] Fetch ...');
})