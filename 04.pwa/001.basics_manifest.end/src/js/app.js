// define message and style
const styles = ['background: green','color: white', 'display: block'].join(';');
/* Declaracion de variables globales */
let MAIN;
let MODAL_POST;
let BTN_SHOW_POST;
let BTN_CANCEL_POST;
// add new variable for deferred prompt
let deferredPrompt;

/**
 * It hides the main page and shows the post modal
 */
const showPostModal = () => {
  MAIN.style.display = 'none';
  MODAL_POST.style.display = 'block';
  setTimeout(() => {
    MODAL_POST.style.transform = 'translateY(0)';
  }, 1);
};

/**
 * It closes the post modal by setting the main element's display to block and the post modal's
 * transform to translateY(100vh)
 */
const closePostModal = () => {
  MAIN.style.display = 'block';
  MODAL_POST.style.transform = 'translateY(100vh)';
};

/* A listener that is executed when the browser detects that the application can be installed. */
window.addEventListener('beforeinstallprompt', (e) => {
  console.info('%c%s', styles, '>>> we block the initial event that proposes the installation of the app');
  e.preventDefault();
  deferredPrompt = e;
})

/* The main function of the application, it is executed when the page is loaded. */
window.addEventListener('load', async () => {
  MAIN = document.querySelector('#main');
  MODAL_POST = document.querySelector('#modal-post-section');
  BTN_SHOW_POST = document.querySelector('#btn-upload-post');
  BTN_SHOW_POST.addEventListener('click', showPostModal);
  BTN_CANCEL_POST = document.querySelector('#btn-post-cancel');
  BTN_CANCEL_POST.addEventListener('click', closePostModal);

  /* install service worker
   **/
  if('serviceWorker' in navigator){
    const response = await navigator.serviceWorker.register('sw.js');
    if(response){
      console.info("Service Worker registered");
    }
  }

  /* event to install PWA application 
   **/
  const bannerInstall = document.querySelector('#banner-install');
  bannerInstall.addEventListener('click', async () => {
    if(deferredPrompt) {
      deferredPrompt.prompt();
      const response = await deferredPrompt.userChoice;
      if(response.outcome === 'dismissed') {
        console.error('the user cancelled the installation')
      }
    }
  })
});