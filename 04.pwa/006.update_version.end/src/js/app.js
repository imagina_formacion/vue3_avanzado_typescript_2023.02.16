// define message and style
const styles = ['background: green', 'color: white', 'display: block'].join(';');
/* Declaracion de variables globales */
let MAIN;
let MODAL_POST;
let BTN_SHOW_POST;
let BTN_CANCEL_POST;
// add new variable for deferred prompt
let deferredPrompt;

/**
 * It hides the main page and shows the post modal
 */
const showPostModal = () => {
  MAIN.style.display = 'none';
  MODAL_POST.style.display = 'block';
  setTimeout(() => {
    MODAL_POST.style.transform = 'translateY(0)';
  }, 1);
};

/**
 * It closes the post modal by setting the main element's display to block and the post modal's
 * transform to translateY(100vh)
 */
const closePostModal = () => {
  MAIN.style.display = 'block';
  MODAL_POST.style.transform = 'translateY(100vh)';
};

/* A listener that is executed when the browser detects that the application can be installed. */
window.addEventListener('beforeinstallprompt', (e) => {
  console.info('%c%s', styles, '>>> we block the initial event that proposes the installation of the app');
  e.preventDefault();
  deferredPrompt = e;
})

/* The main function of the application, it is executed when the page is loaded. */
window.addEventListener('load', async () => {
  MAIN = document.querySelector('#main');
  MODAL_POST = document.querySelector('#modal-post-section');
  BTN_SHOW_POST = document.querySelector('#btn-upload-post');
  BTN_SHOW_POST.addEventListener('click', showPostModal);
  BTN_CANCEL_POST = document.querySelector('#btn-post-cancel');
  BTN_CANCEL_POST.addEventListener('click', closePostModal);

  // we require permissions to issue push notifications
  // await Notification.requestPermission();

  /* install service worker */
  if ('serviceWorker' in navigator) {
    // we can indicate the application subdomain by means of the scope, `{scope: '/'}`
    const response = await navigator.serviceWorker.register('sw.js', { scope: '/' });
    if (response) {
      console.info("Service Worker registered");
      /*
      const ready = await navigator.serviceWorker.ready;
      ready.showNotification('Hola curso pwa ...', {
        body: 'Este será un mensaje mas largo',
        vibrate: [200, 100, 200, 100, 200, 100, 200]
      });
      */

      
      /* A listener that is executed when the service worker is updated. */
      response.addEventListener('updatefound', () => {
        // An updated service worker has appeared in reg.installing!
        newWorker = response.installing;
        newWorker.addEventListener('statechange', () => {
          // Has service worker state changed?
          switch (newWorker.state) {
            case 'installed':
	            // There is a new service worker available, show the notification
              if (navigator.serviceWorker.controller) {
                const data = {
                  message: "A new version of this app is available.",
                  timeout: 5000
                };
                Message('warning').MaterialSnackbar.showSnackbar(data);
                newWorker.postMessage({ action: 'skipWaiting' });
              }
              break;
          }
        });
      });
      //
    }
  }

  window.Message = (option = 'success', container = document.querySelector('#toast-container')) => {
    container.classList.remove('success');
    container.classList.remove('error');
    container.classList.add(option);
    return container;
  }

  /* event to install push notifications */
  const notificationsInstall = document.querySelector('#notifications-install');
  /* It shows a notification to the user. */
  const showNotification = () => {
    navigator.serviceWorker.getRegistration()
      .then( instance => {
        instance.showNotification('notifications activated sucesfully', {
          body: 'body of notification',
          icon: 'src/images/icons/icon-144x144.png',
          image: 'src/images/computer.jpg',
          badge: 'src/images/icons/icon-144x144.png',
          dir: 'ltr',
          tag: 'notifications-postme',
          requireInteraction: true,
          vibrate: [100, 50, 200],
          actions: [
            {action: 'confirm', title: 'accept', icon: 'src/images/icons/icon-144x144.png'},
            {action: 'cancel', title: 'cancel', icon: 'src/images/icons/icon-144x144.png'}
          ]
        })
        .catch(err => console.err(err.message))
      })
  }
  /* It requests permission from the user to send notifications */
  const requestPermission = async () => {
    /* Requesting permission to send notifications to the user. */
    const result = await Notification.requestPermission();
    if (result !== 'granted') {
      const data = {
        message: "user no activated notifications",
        timeout: 5000
      };
      Message('error').MaterialSnackbar.showSnackbar(data);
    } else {
      // configurationSubscription();
      showNotification();
    }
  }
  notificationsInstall.addEventListener('click', requestPermission());

  /* event to install PWA application */
  const bannerInstall = document.querySelector('#banner-install');
  bannerInstall.addEventListener('click', async () => {
    if (deferredPrompt) {
      deferredPrompt.prompt();
      const response = await deferredPrompt.userChoice;
      if (response.outcome === 'dismissed') {
        console.error('the user cancelled the installation')
      }
    }
  })
});