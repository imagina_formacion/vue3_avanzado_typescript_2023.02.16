/* A way to style the console.log message. */
const styles = ['background: orange', 'color: white', 'display: block'].join(';');
const message = `>>> Service Worker file, sw.js, registered`;
// using the styles and message variable
console.info('%c%s', styles, message);

/* Listening for the install event. */
self.addEventListener('install', (event) => {
  console.warn('[SW] 🚧 Installing ...');
  /* in this hook we should cache our files
   **/
  console.warn('[SW] 🏃 Start of file caching ...');
  const wu = new Promise((resolve, reject) => {
    try {
      setTimeout(() => {

        /* in this hook we should cache our files
         **/
        console.warn('[SW] 🚀 Caching files ...');
        resolve();
      }, 1000); // 1 second
      /* we can force the automatic update of new service workers 
       * with the following method:
       **/
      self.skipWaiting();
    } catch (error) {
      reject(error.message);
    }
  })
  // 
  event.waitUntil(wu);
})

/* Listening for the activate event. */
self.addEventListener('activate', (event) => {

  /* in this hook we should remove cached files
  **/
  console.warn('[SW] 🏆 Files successfully cached ...');
  // 
  console.warn('[SW] 😎 Activating ...');
})

/* Listening for the fetch event, allows intercepting requests 
 * coming out of our browser. 
 **/
self.addEventListener('fetch', (event) => {
  console.warn('[SW] 🌐 Fetching ...', event.request.url);
  // return null in each request
  // event.respondWith(null);
  let fetchRequest = fetch(event.request.url);
  event.respondWith(fetchRequest);
})

/* Listening for the sync event and logging it to the console. */
self.addEventListener('sync', (event) => {
  console.warn('[SW] 👨‍💻 Sync ...', event);
})

/* Listening for a push event. */
self.addEventListener('push', (event) => {
  console.warn('[SW] 📌 Push ...', event);
})
