/* A way to style the console.log message. */
const styles = ['background: orange', 'color: white', 'display: block'].join(';');
const message = `>>> Service Worker file, sw.js, registered`;
// using the styles and message variable
console.info('%c%s', styles, message);

// caches alternativas
const CACHE_NAME_CORE='cache-v1';
const CACHE_NAME_INMUTABLE = "inmutable-v1";
const CACHE_NAME_DYNAMIC = "dynamic-v1";

const CACHE_FILES_CORE=[
  "src/images/icons/icon-144x144.png",
  // "https://fonts.googleapis.com/icon?family=Material+Icons",
  // "https://code.getmdl.io/1.3.0/material.brown-orange.min.css_",
  // "https://code.getmdl.io/1.3.0/material.brown-orange.min.css",
  // "https://code.getmdl.io/1.3.0/material.min.js",
  // "https://unpkg.com/pwacompat",
  // "https://fonts.gstatic.com/s/materialicons/v139/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
  "src/css/app.css",
  "src/images/computer.jpg",
  "src/js/app.js",
  "index.html",
  "/"
];

const CACHE_FILES_INMUTABLE = [
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://code.getmdl.io/1.3.0/material.brown-orange.min.css_",
  "https://code.getmdl.io/1.3.0/material.brown-orange.min.css",
  "https://code.getmdl.io/1.3.0/material.min.js",
  "https://unpkg.com/pwacompat",
  "https://fonts.gstatic.com/s/materialicons/v139/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
];


/* eliminar caches antiguas */
caches.keys().then(function(names) {
  for(let name of names){
    if(!name in CACHES){
      caches.delete(name)
    }
  }
})


/* Listening for the install event. */
self.addEventListener('install', (event) => {
  console.warn('[SW] 🚧 Installing ...');
  /* in this hook we should cache our files
   **/
  console.warn('[SW] 🏃 Start of file caching ...'); 
  // abrimos nuestra cache
  /*
  const savingCache = caches.open(CACHE_NAME)
    .then(cache => {
      console.warn('[SW] Cache opened >>>')
      // retornar cada uno de los archivos
      // return CACHE_FILES.forEach(value => cache.add(value))
      return cache.addAll(CACHE_FILES)
    }).catch(
      err => console.error(err.message)
    )
    */
  const savingCache = caches.open(CACHE_NAME_CORE)
     .then(cache => cache.addAll(CACHE_FILES_CORE))
     .catch(err => console.error(err.message))
  const savingInmutable = caches.open(CACHE_NAME_INMUTABLE)
     .then(cache => cache.addAll(CACHE_FILES_INMUTABLE))
     .catch(err => console.error(err.message));
  // 
  self.skipWaiting();
  // event.waitUntil(wu);
  event.waitUntil(savingCache, savingInmutable);

})

/* Listening for the activate event. */
self.addEventListener('activate', (event) => {
  /* in this hook we should remove cached files
  **/
  console.warn('[SW] 🏆 Files successfully cached ...');
  // 
  console.warn('[SW] 😎 Activating ...');
})

/* Listening for the fetch event, allows intercepting requests 
 * coming out of our browser. 
 **/
self.addEventListener('fetch', (event) => {
  console.warn('[SW] 🌐 Fetching ...', event.request.url);
  // chequear las peticiones y trabajar con ellas
  if(!event.request.url.indexOf('http') === 0) {
    return
  }

  /* primera estrategia - solo cache - cachea las request
  const onlyCache = caches.match(event.request);
  event.respondWith(onlyCache);
  */
  /* segunda estrategia - solo network - responde de forma directa
  const onlyNoetwork = fetch(event.request);
  event.respondWith(onlyNoetwork);
  */
  /* tercera estrategia - red soportada por cache
  const cacheHelpNetwork = caches.match(event.request)
    .then(page => page || fetch(event.request));
  event.respondWith(cacheHelpNetwork); 
  */
  /* cuarta estartegia -cache soportada por red */
  /*
  const networkHelpCache = fetch(event.request)
    .then(page => page)
    .catch(caches.match(event.request));
    event.respondWith(networkHelpCache);
  */
  /* cuarta estrategia + respuesta por defecto
    const response = new Response ('This is error part');
    const networkHelpCache = fetch(event.request)
    .then(page => page)
    .catch(networkDied => {
      return caches.match(event.request)
        .then(fileSearched => fileSearched.status != 200)
        .catch(file => response)
    })
    event.respondWith(networkHelpCache);
    */
  /* quinta estrategia */
  const cacheThenNetwork = caches.open(CACHE_NAME_CORE)
  .then(cache => {
    return fetch(event.request)
      .then(response => {
        cache.put(event.request, response.clone());
        return response;
      });
  })
  event.respondWith(cacheThenNetwork);
  

  // return null in each request
  // event.respondWith(null);
  // let fetchRequest = fetch(event.request.url);
  // event.respondWith(fetchRequest);
})

/* Listening for the sync event and logging it to the console. */
self.addEventListener('sync', (event) => {
  console.warn('[SW] 👨‍💻 Sync ...', event);
})

/* Listening for a push event. */
self.addEventListener('push', (event) => {
  console.warn('[SW] 📌 Push ...', event);
})
