/* A way to style the console.log message. */
const styles = ['background: orange', 'color: white', 'display: block'].join(';');
const message = `>>> Service Worker file, sw.js, registered`;
// using the styles and message variable
console.info('%c%s', styles, message);

// cache name with version
const CACHE_NAME = 'cache-v1';
// files 
const CACHE_FILES = [
  "src/images/icons/icon-144x144.png",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  // "https://code.getmdl.io/1.3.0/material.brown-orange.min.css_",
  "https://code.getmdl.io/1.3.0/material.brown-orange.min.css",
  "https://code.getmdl.io/1.3.0/material.min.js",
  "https://unpkg.com/pwacompat",
  "https://fonts.gstatic.com/s/materialicons/v139/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
  "src/css/app.css",
  "src/images/computer.jpg",
  "src/js/app.js",
  "index.html",
  "/"
];

/* Listening for the install event. */
self.addEventListener('install', (event) => {
  console.warn('[SW] 🚧 Installing ...');
  /* in this hook we should cache our files */
  console.warn('[SW] 🏃 Start of file caching ...');
  const savingCache = caches.open(CACHE_NAME) // if the cache exists with that name I return it, if not I create it. 
    .then(cache => {
      console.warn('[SW] 🔓 cache opened >>>');
      // return CACHE_FILES.forEach(value => cache.add(value));
      return cache.addAll(CACHE_FILES);
    }).catch(
      err => console.error(err.message)
    );
  // 
  self.skipWaiting();
  // event.waitUntil() fuerza a esperar la carga
  event.waitUntil(savingCache);
})

/* Listening for the activate event. */
self.addEventListener('activate', (event) => {
  /* in this hook we should remove cached files */
  console.warn('[SW] 🏆 Files successfully cached ...');
  console.warn('[SW] 😎 Activating ...');
})

/* Listening for the fetch event, allows intercepting requests 
 * coming out of our browser. 
 **/
self.addEventListener('fetch', (event) => {
  /* Checking to see if the request is a HTTP request. 
   * If it is not, it will return. 
   **/
  if(!(event.request.url.indexOf('http') === 0)){
    return
  }
  console.warn('[SW] 🌐 Fetching ...');

  /* first strategy - only cache - caching the request. **/
  /* const onlyCache = caches.match(event.request);
  event.respondWith(onlyCache); **/

  /* second strategy - only network - fetching the request and returning it **/
  /* const onlyNetwork = fetch(event.request);
  event.respondWith(onlyNetwork); **/

  /* third strategy - network-supported cache - the above code is checking
   * to see if the page is in the cache. If it is, it will return the page
   * from the cache. If it is not, it will fetch the page from the network.
   **/
  /* const cacheHelpNetwork = caches.match(event.request)
    .then(page => page || fetch(event.request));
  event.respondWith(cacheHelpNetwork); **/

  /* fourth strategy - cache-supported network - Checking the network for 
   * the request, and if it fails, it will return the cached version. 
   * OK = 200, 201, 301,
   * ERROR = 400, 401, 500
   **/
  /* const networkHelpCache = fetch(event.request)
    .then(page => page)
    .catch(caches.match(event.request));  
  event.respondWith(networkHelpCache); **/

  /* const response = new Response('This is error part');
  const networkHelpCache = fetch(event.request)
    .then(page => page)
    .catch(networkDied => {
      return caches.match(event.request)
        .then(fileSearched => fileSearched.status !== 200)
        .catch(file => response)
    });   
  event.respondWith(networkHelpCache); **/
  
  /* fifth strategy - cache then network - Caching the response 
   * from the network and then returning it. 
   **/
  const cacheThenNetwork = caches.open(CACHE_NAME)
    .then(cache => {
      return fetch(event.request)
        .then(response => {
          /* The `cache.put()` method takes two arguments. The first is the request, and the second is
          the response. The response is cloned because the response can only be used once. */
          cache.put(event.request, response.clone());
          return response;
        })
    });
  event.respondWith(cacheThenNetwork);
})

/* Listening for the sync event and logging it to the console. */
self.addEventListener('sync', (event) => {
  console.warn('[SW] 👨‍💻 Sync ...', event);
})

/* Listening for a push event. */
self.addEventListener('push', (event) => {
  console.warn('[SW] 📌 Push ...', event);
})
