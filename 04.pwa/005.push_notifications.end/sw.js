/* A way to style the console.log message. */
const styles = ['background: orange', 'color: white', 'display: block'].join(';');
const message = `>>> Service Worker file, sw.js, registered`;
// using the styles and message variable
console.info('%c%s', styles, message);

// cache name with version
const CACHE_NAME_CORE = 'cache-v1';
const CACHE_NAME_INMUTABLE = 'inmutable-v1';
const CACHE_NAME_DYNAMIC = 'dynamic-v1';
const CACHES = [CACHE_NAME_CORE, CACHE_NAME_INMUTABLE, CACHE_NAME_DYNAMIC]
// 
const CACHE_FILES_CORE = [
  "src/images/icons/icon-144x144.png",
  "src/css/app.css",
  "src/images/computer.jpg",
  "src/js/app.js",
  "index.html",
  "/"
];
const CACHE_FILES_INMUTABLE = [
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://code.getmdl.io/1.3.0/material.brown-orange.min.css",
  "https://code.getmdl.io/1.3.0/material.min.js",
  "https://unpkg.com/pwacompat",
  "https://fonts.gstatic.com/s/materialicons/v139/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
];

/* Removing old caches. */
caches.keys().then(function(names) {
  for (let name of names){
    if(!(name in CACHES)){
      caches.delete(name);
    }
  }
});

/* Listening for the install event. */
self.addEventListener('install', (event) => {
  console.warn('[SW] 🚧 Installing ...');
  console.warn('[SW] 🏃 Start of file caching ...');
  // if the cache exists with that name I return it, if not I create it. 
  const savingCache = caches.open(CACHE_NAME_CORE) 
    .then(cache => cache.addAll(CACHE_FILES_CORE))
    .catch(err => console.error(err.message));
  const savingInmutable = caches.open(CACHE_NAME_INMUTABLE) 
    .then(cache => cache.addAll(CACHE_FILES_INMUTABLE))
    .catch(err => console.error(err.message));
  // 
  self.skipWaiting();
  // event.waitUntil() fuerza a esperar la carga
  event.waitUntil(Promise.all([savingCache, savingInmutable]));
})

/* Listening for the activate event. */
self.addEventListener('activate', (event) => {
  /* in this hook we should remove cached files */
  console.warn('[SW] 🏆 Files successfully cached ...');
  console.warn('[SW] 😎 Activating ...');
})

/* Listening for the fetch event, allows intercepting requests 
 * coming out of our browser. 
 **/
self.addEventListener('fetch', (event) => {
  /* Checking to see if the request is a HTTP request. 
   * If it is not, it will return. 
   **/
  if(!(event.request.url.indexOf('http') === 0)){
    return
  }
  console.warn('[SW] 🌐 Fetching ...');  
  /* fifth strategy - cache then network - Caching the response 
   * from the network and then returning it. 
   **/
  // const cacheThenNetwork = caches.open(CACHE_NAME_CORE)
  const cacheThenNetwork = caches.open(CACHE_NAME_DYNAMIC)
    .then(cache => {
      return fetch(event.request)
        .then(response => {
          cache.put(event.request, response.clone());
          return response;
        })
    });
  event.respondWith(cacheThenNetwork);
})

/* Listening for the sync event and logging it to the console. */
self.addEventListener('sync', (event) => {
  console.warn('[SW] 👨‍💻 Sync ...', event);
})

/* Listening for a push event. */
self.addEventListener('push', (event) => {
  console.warn('[SW] 📌 Push ...', event);
})
