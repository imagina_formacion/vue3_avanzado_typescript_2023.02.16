import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// vite pwa
import { VitePWA } from 'vite-plugin-pwa'
//
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      injectRegister: 'script',
      registerType: 'autoUpdate',
      workbox: {
        clientsClaim: true,
        skipWaiting: true
      },
      devOptions: {
        enabled: true
      },
      includeAssets: ['favicon.ico', 'apple-touch-icon.png', 'masked-icon.svg'],
      manifest: {
        short_name: "Postmen",
        name: "Postmen PWA",
        description: "La aplicación Postmen nos ayudará a compartir nuestros documentos",
        background_color: "#4b2c20",
        theme_color: "#4b2c20",
        orientation: "portrait",
        display: "standalone",
        start_url: "/",
        scope: ".",
        icons: [
          {
            src: "images/icons/icon-144x144.png",
            sizes: "144x144",
            type: "image/png"
          },
          {
            src: "images/icons/icon-192x192.png",
            sizes: "192x192",
            type: "image/png"
          },
          {
            src: "images/icons/icon-256x256.png",
            sizes: "256x256",
            type: "image/png"
          },
          {
            src: "images/icons/icon-384x384.png",
            sizes: "384x384",
            type: "image/png"
          },
          {
            src: "images/icons/icon-512x512.png",
            sizes: "512x512",
            type: "image/png"
          }
        ]
      }
    })
  ],
  // alias
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    }
  },

})
