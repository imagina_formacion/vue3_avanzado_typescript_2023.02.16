import { createApp } from 'vue'
// styles
// import 'vuetify/dist/vuetify.min.css'
import './style.css'
//
import 'vue3-toastify/dist/index.css';
// utils

import App from './App.vue'

createApp(App).mount('#app')
