// define message and style
const styles = ['background: green', 'color: white', 'display: block'].join(';');
//
import { toast } from 'vue3-toastify';
// 
interface BeforeInstallPromptEvent extends Event {
  readonly platforms: string[];
  readonly userChoice: Promise<{
    outcome: "accepted" | "dismissed";
    platform: string;
  }>;
  prompt(): Promise<void>;
}

declare global {
  interface WindowEventMap {
    beforeinstallprompt: BeforeInstallPromptEvent;
  }
}

// add new variable for deferred prompt
var deferredPrompt: BeforeInstallPromptEvent;

/* A listener that is executed when the browser detects that the application can be installed. */
window.addEventListener('beforeinstallprompt', (e) => {
  console.info('%c%s', styles, '>>> we block the initial event that proposes the installation of the app');
  console.log(e)
  e.preventDefault();
  deferredPrompt = e;
})


/* install service worker */
if ('serviceWorker' in navigator) {
  // we can indicate the application subdomain by means of the scope, `{scope: '/'}`
  const response = await navigator.serviceWorker.register('sw.js', { scope: '/' });
  if (response) {
    console.info("Service Worker registered");
    /*
    const ready = await navigator.serviceWorker.ready;
    ready.showNotification('Hola curso pwa ...', {
      body: 'Este será un mensaje mas largo',
      vibrate: [200, 100, 200, 100, 200, 100, 200]
    });
    */
  }
}

/* event to install PWA application */
export const installPWA = async () => {
  if (deferredPrompt) {
    deferredPrompt.prompt();
    const response = await deferredPrompt.userChoice;
    if (response.outcome === 'dismissed') {
      console.error('the user cancelled the installation')
    }
  }
}

/* It requests permission from the user to send notifications */
export const requestPermission = async () => {
  /* Requesting permission to send notifications to the user. */
  const result = await Notification.requestPermission();
  if (result !== 'granted') {
    const data = {
      message: "user no activated notifications",
      timeout: 5000
    };
    // toast('error').MaterialSnackbar.showSnackbar(data);
  } else {
    // configurationSubscription();
    showNotification();
  }
}

/* It shows a notification to the user. */
export const showNotification = () => {
  navigator.serviceWorker.getRegistration()
    .then((instance) => {
      if(instance){
        instance.showNotification('notifications activated sucesfully', {
          body: 'body of notification',
          icon: 'src/images/icons/icon-144x144.png',
          image: 'src/images/computer.jpg',
          badge: 'src/images/icons/icon-144x144.png',
          dir: 'ltr',
          tag: 'notifications-postme',
          requireInteraction: true,
          vibrate: [100, 50, 200],
          actions: [
            { action: 'confirm', title: 'accept', icon: 'src/images/icons/icon-144x144.png' },
            { action: 'cancel', title: 'cancel', icon: 'src/images/icons/icon-144x144.png' }
          ]
        })
          .catch(err => console.error(err.message))
      }
    })
}