import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
//
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  // alias
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    }
  },
  build: {
    assetsInlineLimit: 4096, // default
    // assetsInlineLimit: 0,
    // cssCodeSplit: true, // default, parte los archivos css y sólo los recupera cuando son necesarios
    cssCodeSplit: false, // se trae todo el css en un único archivo
    // sourcemap: false, // default
    // sourcemap: true,
    // sourcemap: "inline",
    sourcemap: "hidden",
  }
});
