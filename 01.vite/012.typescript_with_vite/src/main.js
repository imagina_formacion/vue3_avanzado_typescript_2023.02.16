import { createApp } from "vue";
import App from "./App.vue";

import "./assets/main.css";

// const autoImportedModules = import.meta.glob("./autoImports/*.js");
// console.log(autoImportedModules);
/*
for (const path in autoImportedModules){
    autoImportedModules[path]();
}
*/
const autoImportedModules = import.meta.globEager("./autoImports/*.js");

createApp(App).mount("#app");
