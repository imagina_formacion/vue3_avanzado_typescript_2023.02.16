import { mount } from '@vue/test-utils'
import App from '../../App.vue'
import Form4 from '../04.composition/Form4.vue'

test( 'emits an event only if set value change - find by class', async () => {
  const wrapper = mount( App )
  const wrapperForm = wrapper.findComponent<typeof Form4>('.text-input')

  const email = 'name@mail.com'

  await wrapper.find('.text-input input').setValue(email)

  const updateEvent: string[][]|undefined = wrapperForm.emitted('update:modelValue')

  // ✅ assert the `submit` event is emitted and content is email,
  // console.log(updateEvent)
  expect((updateEvent as string[][])[0][0]).toBe(email)

})

test( 'emits an event only if set value change - find by ref', async () => {
  const wrapper = mount( App )
  const wrapperForm = wrapper.findComponent<typeof Form4>({ref: 'email'})

  const email = 'name@mail.com'

  await wrapperForm.setValue(email)

  const updateEvent: string[][]|undefined = wrapperForm.emitted('update:modelValue')

  // ✅ assert the `submit` event is emitted and content is email
  expect((updateEvent as string[][])[0][0]).toBe(email)

})