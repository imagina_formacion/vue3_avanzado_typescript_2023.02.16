import { mount } from '@vue/test-utils'
import Form from '../01.composition/Form.vue'

/* Testing that the value is being set. */
test('sets the value', async () => {
  const wrapper = mount(Form)
  const input = wrapper.find('input')

  await input.setValue('my@mail.com')
  
  // ✅
  expect(input.element.value).toBe('my@mail.com')
})


/* Testing that the button is being clicked. */
test('trigger', async () => {
  const wrapper = mount(Form)

  // trigger the element
  await wrapper.find('button').trigger('click')

  // ✅ assert some action has been performed, like an emitted event.
  expect(wrapper.emitted()).toHaveProperty('submit')
})


/* Testing the Form component. */
test('emits the input to its parent', async () => {
  const wrapper = mount(Form)

  // set the value
  await wrapper.find('input').setValue('my@mail.com')

  // trigger the element
  await wrapper.find('button').trigger('click')

  // occurrences of `this.$emit('submit')`.
  const submit: string[][]|undefined = wrapper.emitted('submit')

  // ✅ assert the `submit` event is emitted,
  // console.log(submit)
  expect((submit as string[][])[0][0]).toBe('my@mail.com')
})