import { mount } from '@vue/test-utils'
import Form2 from '../02.composition/Form2.vue'

interface Form {
  email: string,
  description: string,
  city: string,
  subscribe: boolean,
  interval: string
}

test( 'submits a form', async () => {
  const wrapper = mount( Form2 )

  const email = 'name@mail.com'
  const description = 'Lorem ipsum dolor sit amet'
  const city = 'moscow'

  await wrapper.find( 'input[type=email]' ).setValue( email )
  await wrapper.find( 'textarea' ).setValue( description )
  await wrapper.find( 'select' ).setValue( city )
  await wrapper.find( 'input[type=checkbox]' ).setValue()
  await wrapper.find( 'input[type=radio][value=monthly]' ).setValue()

  await wrapper.find( 'form' ).trigger( 'submit.prevent' )

  // `emitted()` accepts an argument. It returns an array with all the
  // occurrences of `this.$emit('submit')`.
  const submitEvent: Form[][] | undefined = wrapper.emitted( 'submit' )

  // ✅
  expect( ( submitEvent as Form[][] )[0][0] ).toStrictEqual( {
    email,
    description,
    city,
    subscribe: true,
    interval: 'monthly'
  } )

} )


test( 'handles complex events', async () => {

  /* Mocking the clipboard API. */
  let clipboardContents = ''
  Object.assign( navigator, {
    clipboard: {
      writeText: jest.fn( text => { clipboardContents = text } ),
      readText: jest.fn( () => clipboardContents )
    }
  } )

  /* Creating a wrapper around the component. */
  const wrapper = mount( Form2 )

  const email = 'name@mail.com'
  /* Setting the value of the email input to the email variable. */
  await wrapper.find( 'input[type=email]' ).setValue( email )
  /* It's triggering a keydown event on the email input, preventing the default behavior, and checking
  if the ctrl key is pressed. */
  await wrapper.find( 'input[type=email]' ).trigger( 'keydown.prevent.ctrl.c' )

  // ✅ run your assertions
  expect( navigator.clipboard.readText() ).toBe( '' )
} )