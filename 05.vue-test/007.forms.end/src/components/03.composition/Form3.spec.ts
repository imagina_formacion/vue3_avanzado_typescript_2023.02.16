import { mount } from '@vue/test-utils'
import Form3 from '../03.composition/Form3.vue'

test( 'emits an event only if you lose focus to a button', () => {
  const wrapper = mount( Form3 )

  const componentToGetFocus = wrapper.find( 'button' )

  wrapper.find( 'input' ).trigger( 'blur', {
    relatedTarget: componentToGetFocus.element
  } )

  const onblurEvent: string[][]|undefined = wrapper.emitted('focus-lost')
  
  // ✅
  expect( onblurEvent ).toBeTruthy()
} )