import { mount } from '@vue/test-utils'
import SlotName from './SlotName.vue'

test('layout full page layout', () => {
  const wrapper = mount(SlotName, {
    slots: {
      header: '<div>Header</div>',
      main: '<div>Main Content</div>',
      footer: '<div>Footer</div>'
    }
  })

  // ✅
  expect(wrapper.html()).toContain('<div>Header</div>')
  expect(wrapper.html()).toContain('<div>Main Content</div>')
  expect(wrapper.html()).toContain('<div>Footer</div>')
})