import { mount } from '@vue/test-utils'
import ComponentWithSlots2 from '../04.composition/ComponentWithSlots2.vue'

test('scoped slots', () => {
  const wrapper = mount(ComponentWithSlots2, {
    slots: {
      scoped: `<template #scoped="scope">
        Hello {{ scope.msg }}
        </template>
      `
    }
  })

  // ✅
  expect(wrapper.html()).toContain('Hello world')
})

test('scoped slots', () => {
  const wrapper = mount(ComponentWithSlots2, {
    slots: {
      scoped: `Hello {{ params.msg }}` // no wrapping template tag provided, slot scope exposed as "params"
    }
  })

  // ✅
  expect(wrapper.html()).toContain('Hello world')
})