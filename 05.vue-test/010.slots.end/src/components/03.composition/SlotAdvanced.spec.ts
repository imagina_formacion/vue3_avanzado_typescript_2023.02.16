import { mount } from '@vue/test-utils'
import { h } from 'vue'
import SlotAdvanced from './SlotAdvanced.vue'
import Header from './Header.vue'

test('layout full page layout', () => {
  const wrapper = mount(SlotAdvanced, {
    slots: {
      header: Header,
      main: h('div', 'Main Content'),
      sidebar: { template: '<div>Sidebar</div>' },
      footer: '<div>Footer</div>',
    }
  })

  // ✅
  expect(wrapper.html()).toContain('<div>Header component</div>')
  expect(wrapper.html()).toContain('<div>Main Content</div>')
  expect(wrapper.html()).toContain('<div>Footer</div>')
})