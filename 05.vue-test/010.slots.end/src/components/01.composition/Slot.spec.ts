import { mount } from '@vue/test-utils'
import Slot from './Slot.vue'

test( 'layout default slot', () => {
  const wrapper = mount( Slot, {
    slots: {
      default: 'Main Content'
    }
  } )

  // ✅
  expect( wrapper.html() ).toContain( 'Main Content' )
  expect( wrapper.find( 'main' ).text() ).toContain( 'Main Content' )

} )

test( 'layout full page layout', () => {
  const wrapper = mount( Slot, {
    slots: {
      default: [
        '<div id="one">One</div>',
        '<div id="two">Two</div>'
      ]
    }
  } )

  // ✅
  expect( wrapper.find( '#one' ).exists() ).toBe( true )
  expect( wrapper.find( '#two' ).exists() ).toBe( true )
} )