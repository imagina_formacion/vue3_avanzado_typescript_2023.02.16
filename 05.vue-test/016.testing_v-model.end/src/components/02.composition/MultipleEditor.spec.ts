import { mount } from '@vue/test-utils'

import MoneyEditor from './MoneyEditor.vue'

test('modelValue and currency should be updated', async () => {
  const wrapper = mount(MoneyEditor, {
    props: {
      modelValue: 1,
      'onUpdate:modelValue':  (e: Event) => wrapper.setProps({ modelValue: e }),
      currency: '$',
      'onUpdate:currency':  (e: Event) => wrapper.setProps({ currency: e })
    }
  })

  const [currencyInput, modelValueInput] = wrapper.findAll('input')

  expect(wrapper.props('modelValue')).toBe(1)
  expect(wrapper.props('currency')).toBe('$')

  await modelValueInput.setValue(10)
  await currencyInput.setValue('£')

  expect(wrapper.props('modelValue')).toBe(10)
  expect(wrapper.props('currency')).toBe('£')
})