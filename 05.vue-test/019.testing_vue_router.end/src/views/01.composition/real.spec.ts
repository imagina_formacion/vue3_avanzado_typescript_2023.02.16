import { mount } from '@vue/test-utils'
import { createRouter, createWebHistory } from 'vue-router'
import { routes } from "@/router"
import App from '@/App.vue'
import View from '@/views/01.composition/View.vue'
import Edit from '@/views/01.composition/Edit.vue'

let router: any;

beforeEach(async () => {
  router = createRouter({
    history: createWebHistory(),
    routes: routes,
  })
});

test('render component 1', async () => {
  router.push('/post/1')
  await router.isReady()
  
  const wrapper = mount(App, {
    global: {
      plugins: [router],
    }
  })

  const viewWrapper = wrapper.findComponent(View)

  expect(viewWrapper.exists()).toBe(true)
  
  expect(wrapper.find('button').exists()).toBe(true)

  wrapper.find('button').trigger('click') 

})


test('render component 2', async () => {
  // navigate to route
  router.push('/post/1/edit')
  // await navigation from push()
  await router.isReady()

  const wrapper = mount(App, {
    global: {
      plugins: [router],
    }
  })

  const editWrapper = wrapper.findComponent(Edit)

  expect(editWrapper.exists()).toBe(true)
})