import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import HomeView from '@/views/HomeView.vue'
import Edit from '@/views/01.composition/Edit.vue'
import View from '@/views/01.composition/View.vue'
import NotFound from '@/views/01.composition/NotFound.vue'

export const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/post/:id/edit',
    name: 'Edit',
    component: Edit
  },
  {
    path: '/post/:id',
    name: 'post',
    component: View
  },
  {
    path: '/404',
    name: 'not-found',
    component: NotFound
  },
]

export const router = createRouter({
  history: createWebHistory(),
  routes
})


export default router