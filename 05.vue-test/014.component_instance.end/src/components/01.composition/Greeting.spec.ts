import { mount } from '@vue/test-utils'
import Greeting from './Greeting.vue'

test('renders a greeting', () => {

  const wrapper = mount(Greeting, {
    props: {
      msg1: 'hello'
    }
  })

  // console.log(wrapper)

  expect(wrapper.html()).toContain('hello world')
})