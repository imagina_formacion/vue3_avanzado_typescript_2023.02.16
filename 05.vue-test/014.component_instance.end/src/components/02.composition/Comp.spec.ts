import { mount } from '@vue/test-utils'
import Comp from './Comp.vue'
import Foo from './Foo.vue'

test('asserts correct props are passed', () => {

  const wrapper = mount(Comp)
  
  expect(wrapper.getComponent(Foo).vm.msg).toBe('hello world')
  expect(wrapper.getComponent(Foo).props()).toEqual({ msg: 'hello world' })
})