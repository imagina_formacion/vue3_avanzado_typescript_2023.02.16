import { mount, flushPromises } from '@vue/test-utils'
import axios from 'axios'
import PokemonList from './PokemonList.vue'

const mockPokemonList = {
  data: {
    results: [
  { id: 1, name: 'bulbasaur' },
  { id: 2, name: 'ivysaur' },
  { id: 3, name: 'venusaur' },
  { id: 4, name: 'charmander' },
  { id: 5, name: 'charmeleon' },
  { id: 6, name: 'charizard' },
  { id: 7, name: 'squirtle' },
  { id: 8, name: 'wartortle' },
  { id: 9, name: 'blastoise' },
  { id: 10, name: 'caterpie' },
  { id: 11, name: 'metapod' },
  { id: 12, name: 'butterfree' },
  { id: 13, name: 'weedle' },
  { id: 14, name: 'kakuna' },
  { id: 15, name: 'beedrill' },
  { id: 16, name: 'pidgey' },
  { id: 17, name: 'pidgeotto' },
  { id: 18, name: 'pidgeot' },
  { id: 19, name: 'rattata' },
  { id: 20, name: 'raticate' },
  ]
}}

// Following lines tell Jest to mock any call to `axios.get`
// and to return `mockPokemonList` instead
jest.spyOn(axios, 'get').mockResolvedValue(mockPokemonList)

test('loads pokemons on button click', async () => {
  const wrapper = mount(PokemonList)

  await wrapper.get('button').trigger('click')

  // Let's assert that we've called axios.get the right amount of times and
  // with the right parameters.
  expect(axios.get).toHaveBeenCalledTimes(1)
  expect(axios.get).toHaveBeenCalledWith('https://pokeapi.co/api/v2/pokemon')

  // Wait until the DOM updates.
  await flushPromises()

  // Finally, we make sure we've rendered the content from the API.
  const pokemons = wrapper.findAll('[data-test="pokemon"]')

  expect(pokemons).toHaveLength(20)
  expect(pokemons[0].text()).toContain('bulbasaur')
  expect(pokemons[1].text()).toContain('ivysaur')
})