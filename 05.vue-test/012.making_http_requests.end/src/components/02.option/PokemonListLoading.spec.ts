import { mount, flushPromises } from '@vue/test-utils'
import axios from 'axios'
import PokemonListLoading from './PokemonListLoading.vue'

const mockPokemonList2 = {
  data: {
    results: [
  { id: 1, name: 'bulbasaur' },
  { id: 2, name: 'ivysaur' },
  { id: 3, name: 'venusaur' },
  { id: 4, name: 'charmander' },
  { id: 5, name: 'charmeleon' },
  { id: 6, name: 'charizard' },
  { id: 7, name: 'squirtle' },
  { id: 8, name: 'wartortle' },
  { id: 9, name: 'blastoise' },
  { id: 10, name: 'caterpie' },
  { id: 11, name: 'metapod' },
  { id: 12, name: 'butterfree' },
  { id: 13, name: 'weedle' },
  { id: 14, name: 'kakuna' },
  { id: 15, name: 'beedrill' },
  { id: 16, name: 'pidgey' },
  { id: 17, name: 'pidgeotto' },
  { id: 18, name: 'pidgeot' },
  { id: 19, name: 'rattata' },
  { id: 20, name: 'raticate' },
  ]
}}

// Following lines tell Jest to mock any call to `axios.get`
// and to return `mockPokemonList2` instead
jest.spyOn(axios, 'get').mockResolvedValue(mockPokemonList2)

test('displays loading state on button click', async () => {
  const wrapper = mount(PokemonListLoading)
  console.log(wrapper.find('[role="alert"]').exists())

  // Notice that we run the following assertions before clicking on the button
  // Here, the component should be in a "not loading" state.
  expect(wrapper.find('[role="alert"]').exists()).toBe(false)
  expect(wrapper.get('button').attributes()).not.toHaveProperty('disabled')

  // Now let's trigger it as usual.
  await wrapper.get('button').trigger('click')

  // We assert for "Loading state" before flushing all promises.
  expect(wrapper.find('[role="alert"]').exists()).toBe(true)
  expect(wrapper.get('button').attributes()).toHaveProperty('disabled')

  // As we did before, wait until the DOM updates.
  await flushPromises()

  // After that, we're back at a "not loading" state.
  expect(wrapper.find('[role="alert"]').exists()).toBe(false)
  expect(wrapper.get('button').attributes()).not.toHaveProperty('disabled')
})