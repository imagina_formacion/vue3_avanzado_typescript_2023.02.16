import { mount } from '@vue/test-utils'
import Counter2 from '../02.composition/Counter2.vue'

test('emits an event when clicked and return correct data', () => {
  const wrapper = mount(Counter2)

  wrapper.find('button').trigger('click')
  wrapper.find('button').trigger('click')
  
  // `emitted()` accepts an argument. It returns an array with all the
  // occurrences of `this.$emit('increment')`.
  const incrementEvent:  {
    count: number,
    isEven: boolean 
  }[]|undefined = wrapper.emitted('increment')

  // ✅ We have "clicked" twice, so the array of `increment` should
  // have two values.
  expect(incrementEvent).toHaveLength(2)

  // ✅ Assert the result of the first click.
  // Notice that the value is an array.
  expect((incrementEvent as {
    count: number,
    isEven: boolean 
  }[])[0]).toEqual([{
    count: 1,
    isEven: false
  }])

  // ✅ Then, the result of the second one.
  expect((incrementEvent as {
    count: number,
    isEven: boolean 
  }[])[1]).toEqual([{
    count: 2,
    isEven: true
  }])
})