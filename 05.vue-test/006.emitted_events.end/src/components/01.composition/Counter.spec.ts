import { mount } from '@vue/test-utils'
import Counter from '../01.composition/Counter.vue'

test('emits an event when clicked', () => {
  const wrapper = mount(Counter)

  wrapper.find('button').trigger('click')
  wrapper.find('button').trigger('click')
  
  // ✅
  expect(wrapper.emitted()).toHaveProperty('increment')
})


test('emits an event when clicked and return correct data', () => {
  const wrapper = mount(Counter)

  wrapper.find('button').trigger('click')
  wrapper.find('button').trigger('click')
  
  // `emitted()` accepts an argument. It returns an array with all the
  // occurrences of `this.$emit('increment')`.
  const incrementEvent: number[]|undefined = wrapper.emitted('increment')

  // ✅ We have "clicked" twice, so the array of `increment` should
  // have two values.
  expect(incrementEvent).toHaveLength(2)

  // ✅ Assert the result of the first click.
  // Notice that the value is an array.
  expect((incrementEvent as number[])[0]).toEqual([1])

  // ✅ Then, the result of the second one.
  expect((incrementEvent as number[])[1]).toEqual([2])
})