import { mount } from '@vue/test-utils'
import {ref} from 'vue'
import { defineStore } from 'pinia'
import { createTestingPinia } from '@pinia/testing'
// import any store you want to interact with in tests
import { useCounterStore } from '@/stores/01.composition/counter'
import App from '@/App.vue'

describe('moking getters', () => {

  const useCounter = defineStore('counter', {
    state: () => ({ n: 1 }),
    getters: {
      double: (state) => state.n * 2,
    },
  })

  it('mock pinia', () => {
    const pinia = createTestingPinia()
    const counter = useCounter(pinia)

    console.log(counter.double)
  })

})