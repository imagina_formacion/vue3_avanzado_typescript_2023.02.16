import { mount } from '@vue/test-utils'
import {ref} from 'vue'
import { createTestingPinia } from '@pinia/testing'
// import any store you want to interact with in tests
import { useCounterStore } from '@/stores/01.composition/counter'
import App from '@/App.vue'

describe('my Store', () => {

  it('set data', () => {
    const wrapper = mount(App, {
      global: {
        plugins: [
          createTestingPinia({
            initialState: {
              counter: { count: 10 }, // start the counter at 20 instead of 0
            },
          }),
        ],
      },
    })
    // 
    // console.log(wrapper.html())
  })

  it('set data', () => {
    const store = useCounterStore() // uses the testing pinia!
    store.count = 20// 20
    console.log(store.count)
  })
})