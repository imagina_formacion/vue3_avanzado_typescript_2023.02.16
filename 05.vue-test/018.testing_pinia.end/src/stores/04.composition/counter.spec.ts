import { mount } from '@vue/test-utils'
import {ref} from 'vue'
import { createTestingPinia } from '@pinia/testing'
// import any store you want to interact with in tests
import { useCounterStore } from '@/stores/01.composition/counter'
import App from '@/App.vue'

describe('my Store stub actions', () => {

  it('set data', () => {
    const wrapper = mount(App, {
      global: {
        plugins: [
          createTestingPinia({
            initialState: {
              counter: { count: 10 }, // start the counter at 20 instead of 0
            },
            stubActions: false
          }),
        ],
      },
    })
    // 
    console.log(wrapper.html())
  })

  it('increment method was called', () => {
    const store = useCounterStore() // uses the testing pinia!
    // Now this call WILL execute the implementation defined by the store
    store.increment()

    // ...but it's still wrapped with a spy, so you can inspect calls
    expect(store.increment).toHaveBeenCalledTimes(1)
  })
})