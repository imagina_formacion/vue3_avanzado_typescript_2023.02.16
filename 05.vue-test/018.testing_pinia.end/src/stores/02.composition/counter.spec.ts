import { mount } from '@vue/test-utils'
import { createTestingPinia } from '@pinia/testing'
// import any store you want to interact with in tests
import { useCounterStore } from '@/stores/01.composition/counter'
import Counter from '@/components/02.composition/Counter.vue'

describe('my Store', () => {
  const wrapper = mount(Counter, {
    global: {
      plugins: [createTestingPinia()],
    },
  })

  const store = useCounterStore() // uses the testing pinia!

  it('set data', () => {
    // state can be directly manipulated
    store.count = 10
    expect(store.count).toBe(10)
    // can also be done through patch
    store.$patch({ count: 112 })
    expect(store.count).toBe(112)
  })

  it('execute method', () => {
    // actions are stubbed by default, meaning they don't execute their code by default.
    // See below to customize this behavior.
    store.increment()

    expect(store.increment).toHaveBeenCalledTimes(1)
    expect(store.increment).toHaveBeenLastCalledWith()
  })
})