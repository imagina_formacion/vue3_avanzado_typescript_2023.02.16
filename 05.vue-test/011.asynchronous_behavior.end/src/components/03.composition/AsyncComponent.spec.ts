import { mount, flushPromises } from '@vue/test-utils'
import axios from 'axios'
import AsyncComponent from './AsyncComponent.vue'

const data = {
  data: {
    base_experience: 101
  }
}
jest.spyOn(axios, 'get').mockResolvedValue(data)

test('Async component', async () => {
  // some component that makes a HTTP called in `created` using `axios`
  const wrapper = mount(AsyncComponent)

  await flushPromises() // axios promise is resolved immediately
  
  // console.log(wrapper.html())
  // after the line above, axios request has resolved with the mocked data.
  // ✅
  expect(wrapper.html()).toContain('ditto tiene :101 de experiencia')
})