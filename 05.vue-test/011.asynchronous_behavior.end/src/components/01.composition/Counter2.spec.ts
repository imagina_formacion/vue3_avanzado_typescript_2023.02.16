import { mount } from '@vue/test-utils'
import { nextTick } from 'vue'
import Counter2 from './Counter2.vue'

// ❌ needs async to find the element
test('increments by 1', () => {
  const wrapper = mount(Counter2)

  wrapper.find('button').trigger('click')

  // ❌
  // expect(wrapper.html()).toContain('Count: 1')
})

// ✅ 
test('increments by 1 - async', async () => {
  const wrapper = mount(Counter2)

  await wrapper.find('button').trigger('click')

  // ✅
  expect(wrapper.html()).toContain('Count: 1')
})

// ✅ 
test('increments by 1 - async nextTick', async () => {
  const wrapper = mount(Counter2)

  wrapper.find('button').trigger('click')

  await nextTick()

  // ✅
  expect(wrapper.html()).toContain('Count: 1')
})

