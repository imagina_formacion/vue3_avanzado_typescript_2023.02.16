import { mount } from '@vue/test-utils'
import { nextTick } from 'vue'
import Counter from './Counter.vue'

test('increments by 1', () => {
  const wrapper = mount(Counter)

  wrapper.find('button').trigger('click')

  // ❌
  // expect(wrapper.html()).toContain('Count: 1')
})

test('increments by 1 - async nextTick', async () => {
  const wrapper = mount(Counter)

  wrapper.find('button').trigger('click')

  await nextTick()

  // ✅
  expect(wrapper.html()).toContain('Count: 1')
})

test('increments by 1 - async', async () => {
  const wrapper = mount(Counter)

  await wrapper.find('button').trigger('click')

  // ✅
  expect(wrapper.html()).toContain('Count: 1')
})