```bash
npm install --save-dev @vue/test-utils
```

## 💡 Conclusión

* Usa `mount()` para renderizar un componente.
* Usa `get()` y `findAll()` para consultar el DOM.
* `trigger()` y `setValue()` son ayudantes para simular la entrada del usuario.
* Actualizar el DOM es una operación asíncrona, así que asegúrese de usar `async` y `await`.
* Las pruebas suelen constar de 3 fases; ordenar, actuar y afirmar.