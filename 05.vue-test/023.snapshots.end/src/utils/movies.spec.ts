import movies from './movies'

interface Movie {
    title: string,
    rate: null|number
}

describe('Favorite Movies', () => {
  let myMovies: Movie[] = []
  beforeEach(() => {
    myMovies = [{
      title: 'Age of Ultron',
      rate: null
    }]
  })

  test('can add a movie', () => {
    movies.add(myMovies, 'Kung Fury')
    expect(myMovies).toMatchSnapshot()
  })

  test('rate a movie', () => {
    movies.rate(myMovies[0], 5)
    expect(myMovies).toMatchSnapshot()
  })

  /*
  test('can add a movie and rate a movie', () => {
    movies.add(myMovies, 'Avatar')
    expect(myMovies).toMatchSnapshot()
    movies.rate(myMovies[1], 4)
    // console.log(myMovies)
    expect(myMovies).toMatchSnapshot()
  })
  */
})