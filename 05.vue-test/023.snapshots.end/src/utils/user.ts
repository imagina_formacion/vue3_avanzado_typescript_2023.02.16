export default class User {

  firstname: string;
  lastname: string;

  constructor(details: { firstname: string, lastname: string }) {
    const { firstname, lastname } = details
    this.firstname = firstname
    this.lastname = lastname
  }
  
  get name() {
    return `${this.firstname} ${this.lastname}`
  }
}