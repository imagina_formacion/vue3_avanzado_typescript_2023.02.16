
interface Movie {
  title: string,
  rate: null | number
}

export default {
  add(collection: Movie[], movie: string) {
    collection.push({
      title: movie,
      rate: null
    })
  },

  rate(obj: Movie, rating: number) {
    obj.rate = rating
  }
}