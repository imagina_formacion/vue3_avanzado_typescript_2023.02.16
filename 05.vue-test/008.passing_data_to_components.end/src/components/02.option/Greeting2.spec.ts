import { mount } from '@vue/test-utils'
import Greeting2 from './Greeting2.vue'

test('renders a greeting when Greeting2 is false', async () => {
  const wrapper = mount(Greeting2)
  expect(wrapper.html()).toContain('Hello')

  await wrapper.setProps({ show: false })

  let html: string = wrapper.html( { raw: true } );
  // console.log(html)
  // ✅ 
  expect(html).not.toContain('Hello')
})

test('renders a greeting when Greeting2 is true', async () => {
  const wrapper = mount(Greeting2)
  expect(wrapper.html()).toContain('Hello')

  await wrapper.setProps({ show: true })

  let html: string = wrapper.html( { raw: true } );
  // console.log(html)
  // ✅ 
  expect(html).toContain('Hello')
})