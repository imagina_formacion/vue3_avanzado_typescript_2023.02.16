import { mount } from '@vue/test-utils'
import Password2 from '../01.option/Password2.vue'

test( 'renders an error if length is too short', () => {

  const minLength = 10

  const wrapper = mount( Password2, {
    props: {
      minLength: minLength
    },
    data() {
      return {
        password: 'short'
      }
    }
  } )

  let html: string = wrapper.html( { raw: true } );
  // console.log(html)
  // ✅ 
  expect( html ).toContain( `Password must be at least ${minLength} characters.` )

} )