import { mount } from '@vue/test-utils'
import Greeting from './Greeting.vue'

test('renders a greeting when Greeting is false', async () => {
  const wrapper = mount(Greeting)
  expect(wrapper.html()).toContain('Hello')

  await wrapper.setProps({ show: false })

  let html: string = wrapper.html( { raw: true } );
  // console.log(html)
  // ✅ 
  expect(html).not.toContain('Hello')
})

test('renders a greeting when Greeting is true', async () => {
  const wrapper = mount(Greeting)
  expect(wrapper.html()).toContain('Hello')

  await wrapper.setProps({ show: true })

  let html: string = wrapper.html( { raw: true } );
  // console.log(html)
  // ✅ 
  expect(html).toContain('Hello')
})