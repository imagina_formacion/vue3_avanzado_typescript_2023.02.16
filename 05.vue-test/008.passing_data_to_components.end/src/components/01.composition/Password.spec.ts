import { mount } from '@vue/test-utils'
import Password from './Password.vue'

test( 'renders an error if length is too short', async () => {

  const minLength = 10
  const password = 'short'

  const wrapper = mount(Password, {
    props: {
      minLength: minLength
    }
  })

  wrapper.find( 'input' ).setValue( password )

  let html: string = wrapper.html( { raw: true } );
  // console.log(html)
  // ✅ 
  expect( html ).toContain( `Password must be at least ${minLength} characters.` )

} )