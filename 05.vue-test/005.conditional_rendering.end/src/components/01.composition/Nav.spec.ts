import { mount } from '@vue/test-utils'
import Nav from '../01.composition/Nav.vue'

/* Testing that the profile link exists. */
test( 'renders a profile link - using get', () => {
  const wrapper = mount( Nav )
  /* ✅ Asserting that the element with the id of profile exists and that the text of that element is equal
  to the string 'My Profile'. */
  const profileLink = wrapper.get( '#profile' )
  expect( profileLink.text() ).toEqual( 'My Profile' )

} )

/* Testing that the admin link exists. */
test( 'renders an admin link - using exists', () => {
  const wrapper = mount( Nav )
  /* ✅ Asserting that the element with the id of admin does not exist. */
  expect( wrapper.find( '#admin' ).exists() ).toBe( false )
} )

/* Testing that the admin link exists. */
test( 'renders an admin link - using data', () => {
  const wrapper = mount( Nav, {
    setup() {
      return {
        admin: true
      }
    }
  } )
  /* ✅ Asserting that the text of the element with the id of admin is equal to the string 'Admin'. */
  expect( wrapper.get( '#admin' ).text() ).toEqual( 'Admin' )
} )


