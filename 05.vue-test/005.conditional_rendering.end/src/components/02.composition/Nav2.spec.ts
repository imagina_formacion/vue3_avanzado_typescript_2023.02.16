import { mount } from '@vue/test-utils'
import Nav2 from '../02.composition/Nav2.vue'

/* Asserting that the element #profile exists. */
test( 'renders a profile link', () => {
  const wrapper = mount( Nav2 )
  // Here we are implicitly asserting that the
  // element #profile exists.
  const profileLink = wrapper.get( '#profile' )
  // ✅
  expect( profileLink.text() ).toEqual( 'My Profile' )
})

/* Asserting that the element #user-dropdown exists. */
test('no render an user-dropdown', () => {
  const wrapper = mount( Nav2 )
  // ❌ using v-show will always render the element
  expect( wrapper.find( '#user-dropdown' ).exists() ).toBe( true )
} )

/* Asserting that the element #user-dropdown exists. */
test('renders an user-dropdown', () => {
  const wrapper = mount(Nav2, {
    setup() {
      return {
        shouldShowDropdown: true
      }
    }
  })

  /* ✅ Asserting that the element #user-dropdown exists. */
  expect( wrapper.find( '#user-dropdown' ).exists() ).toBe( true )
})

/* Testing that the user dropdown is not visible. */
test('does not show the user dropdown', () => {
  const wrapper = mount(Nav2)
  /* ✅ Asserting that the user dropdown is not visible. */
  expect(wrapper.get('#user-dropdown').isVisible()).toBe(false)
})