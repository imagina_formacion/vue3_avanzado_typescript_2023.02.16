import { mount } from '@vue/test-utils'

import SomeChild from './SomeChild.vue'
import ParentComponent from './ParentComponent.vue'
import TestComponent from './TestComponent.vue'

test('provides correct data - stubs', () => {

  const wrapper = mount(ParentComponent, {
    global: {
      stubs: {
        SomeChild: TestComponent
      }
    }
  })

  // console.log(wrapper.html())
  expect(wrapper.find('#provide-test').text()).toBe('some-data')
})