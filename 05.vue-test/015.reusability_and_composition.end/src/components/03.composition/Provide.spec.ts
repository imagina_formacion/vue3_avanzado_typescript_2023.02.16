import { mount } from '@vue/test-utils'

import ParentComponent from './ParentComponent.vue'
import TestComponent from './TestComponent.vue'

test('provides correct data - provide', () => {

  const wrapper = mount(ParentComponent, {
    slots: {
      default: TestComponent
    }
  })

  expect(wrapper.find('#provide-test').text()).toBe('some-data')
})