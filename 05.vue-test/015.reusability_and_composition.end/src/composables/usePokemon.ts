import {ref, onMounted} from 'vue'

import axios from 'axios'

export function usePokemon(pokemonId: number) {

  const pokemon = ref<{
    name: string
  }|null>()
  
  function fetchPokemon(id: number) {
    axios.get(`https://pokeapi.co/api/v2/pokemon/${id}`)
      .then(response => (pokemon.value = response.data))
  }

  onMounted(() => fetchPokemon(pokemonId))

  return { pokemon }
}