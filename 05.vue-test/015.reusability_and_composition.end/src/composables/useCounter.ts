import {ref} from 'vue'

export function useCounter() {
  const counter = ref(0)

  function increase() {
    console.log(counter.value)
    counter.value += 1
  }

  return { counter, increase }
}