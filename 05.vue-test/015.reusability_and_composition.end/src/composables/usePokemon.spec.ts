import { mount, flushPromises } from '@vue/test-utils'
import axios from 'axios'
import Pokemon from '@/components/02.composition/Pokemon.vue'
// Mock API request
jest.spyOn(axios, 'get').mockResolvedValue({ data: { name: 'bulbasaur' } })

test('fetch user on mount', async () => {
const pokemonId: number = 1
  const wrapper = mount(Pokemon, {
    props: {
      pokemonId: pokemonId
    }
  })

  expect(wrapper.vm.pokemon).toBeUndefined()

  await flushPromises()

  expect(wrapper.vm.pokemon).toEqual({ name: 'bulbasaur' })
})