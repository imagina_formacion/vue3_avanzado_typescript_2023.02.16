import { mount } from '@vue/test-utils'
import Counter2 from '../01.composition/Counter2.vue'

/* Testing that the text of the paragraph is 'Times clicked: 0' */
test('counter2 text updates', async () => {
  const wrapper = mount(Counter2)
  const paragraph = wrapper.find('.paragraph')

  // ✅ 
  expect(paragraph.text()).toBe('Times clicked: 0')
})

/* Setting the count to 2 and then checking that the text is 'Times clicked: 2' */
test('counter2 text updates with count 2', async () => {
  const wrapper = mount(Counter2, {
    setup(){
      return {
        count: 2
      }
    }
  })
  const paragraph = wrapper.find('.paragraph')

  // ✅ 
  expect(paragraph.text()).toBe('Times clicked: 2')
})

/* - Mounting the component
- Finding the button
- Triggering a click event on the button
- Checking the text of the component */
test('text updates on clicking', async () => {
  const wrapper = mount(Counter2)

  // ✅ 
  expect(wrapper.text()).toContain('Times clicked: 0')

  const button = wrapper.find('button')
  await button.trigger('click')
  await button.trigger('click')

  // ✅ 
  expect(wrapper.text()).toContain('Times clicked: 2')
})