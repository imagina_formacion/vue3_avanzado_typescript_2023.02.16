import { mount } from '@vue/test-utils'
import TodoApp3 from '../03.composition/TodoApp3.vue'

test('completes a todo3', async () => {
  // 🗄️ Organise
  const wrapper = mount(TodoApp3)
  // 🎭 Act
  await wrapper.get('[data-test="todo-checkbox"]').setValue(true)
  // ✅ Assert
  expect(wrapper.get('[data-test="todo"]').classes()).toContain('completed')
})