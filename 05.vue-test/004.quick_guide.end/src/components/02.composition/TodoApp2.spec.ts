import { mount } from '@vue/test-utils'
import TodoApp2 from '../02.composition/TodoApp2.vue'

// ❌ needs async to find the element
/*
test('creates a todo', () => {
  // 🗄️ Organise
  const wrapper = mount(TodoApp2)
  // ✅ Assert
  expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(1)

  // 🎭 Act
  wrapper.get('[data-test="new-todo"]').setValue('New todo')
  wrapper.get('[data-test="form"]').trigger('submit')

  // ✅ Assert
  expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(2)
})

*/

// ✅ 
test('creates a todo2', async () => {
  // 🗄️ Organise
  const wrapper = mount(TodoApp2)
  // ✅ Assert
  expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(1)
  // 🎭 Act
  await wrapper.get('[data-test="new-todo"]').setValue('New todo')
  await wrapper.get('[data-test="form"]').trigger('submit')
  // ✅ Assert
  expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(2)
})