import { mount } from '@vue/test-utils'
import TodoApp from '../01.composition/TodoApp.vue'

test('renders a todo', () => {
  // 🗄️ Organise
  const wrapper = mount(TodoApp)

  const todo = wrapper.get('[data-test="todo"]')
  // ✅ Assert
  expect(todo.text()).toBe('Learn Vue.js 3')
})