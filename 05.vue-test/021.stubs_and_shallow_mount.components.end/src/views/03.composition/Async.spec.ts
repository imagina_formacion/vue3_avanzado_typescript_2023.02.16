import { mount, flushPromises} from '@vue/test-utils'
import Async from '@/views/03.composition/Async.vue'

test('stubs async component without resolving', () => {
  const wrapper = mount(Async, {
    global: {
      stubs: {
        MyComponent: true
      }
    }
  })

  // 
  console.log(wrapper.html())
  expect(wrapper.html()).toBe('<my-component-stub></my-component-stub>')
})

/* 
 * este test podría ser obviado ya que al utilizar un stub no es necesario esperar
 * a la respuesta de la promesa
 **/
test('stubs async component with resolving', async () => {
  const wrapper = mount(Async, {
    global: {
      stubs: {
        MyComponent: true
      }
    }
  })

  await flushPromises()

  // 
  console.log(wrapper.html())
  // 

  expect(wrapper.html()).toBe('<my-component-stub></my-component-stub>')
})
