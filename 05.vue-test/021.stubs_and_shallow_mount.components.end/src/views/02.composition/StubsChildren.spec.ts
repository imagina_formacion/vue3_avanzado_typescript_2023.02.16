import { mount, flushPromises } from '@vue/test-utils'
import StubsChildren from '@/views/02.composition/StubsChildren.vue'

test( 'stubs component with custom templates', () => {
  const wrapper = mount( StubsChildren, {
    global: {
      stubs: {
        ComplexA: true,
        ComplexB: true,
        ComplexC: true
      }
    }
  } )

  console.log( wrapper.html() )
  // <h1>Welcome to Vue.js 3</h1><span></span>

  expect( wrapper.html() ).toContain( 'Welcome to Vue.js 3' )
} )

test( 'using sallow', () => {
  const wrapper = mount( StubsChildren, {
    shallow: true
  } )

  console.log( wrapper.html() )
  /*
    <h1>Welcome to Vue.js 3</h1>
    <complex-a-stub></complex-a-stub>
    <complex-b-stub></complex-b-stub>
    <complex-c-stub></complex-c-stub>
  */

  expect( wrapper.html() ).toContain( 'Welcome to Vue.js 3' )
} )

const ComplexA = {
  template: '<h2>Hello from real component!</h2>'
}


test( 'shallow allows opt-out of stubbing specific component', () => {
  const wrapper = mount( StubsChildren, {
    shallow: true,
    global: {
      stubs: { ComplexA: false }
    }
  } )

  console.log( wrapper.html() )
  /*
    <h1>Welcome to Vue.js 3</h1>
    <h2>Hello from real component!</h2>
    <complex-b-stub></complex-b-stub>
    <complex-c-stub></complex-c-stub>
  */


  expect( wrapper.html() ).toContain( 'Welcome to Vue.js 3' )
} )
