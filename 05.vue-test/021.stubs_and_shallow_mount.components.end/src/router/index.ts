import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import Pokemons from '@/views/01.composition/Pokemons.vue'
import StubsChildren from '@/views/02.composition/StubsChildren.vue'
import Async from '@/views/03.composition/Async.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'pokemons',
    component: Pokemons
  },
  {
    path: '/stubs-children',
    name: 'StubsChildren',
    component: StubsChildren
  },
  {
    path: '/async',
    name: 'Async',
    component: Async
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
