import {defineStore} from 'pinia'
import {useAuthStore} from '@/stores/auth'
import {useSettingsStore} from '@/stores/setting'
interface User {
  username: string,
  logged: boolean
}

interface State {
  userData: User
}

const registerUser = (username: string, password: string): Promise<User|null> => {
  return new Promise(function (resolve, reject) {
    setTimeout(() => {
      const auth = useAuthStore()
      const settings = useSettingsStore();
      if(username == 'Dio' && password == 'irejectmyhumanityjojo'){
        auth.isAuthenticated = true
        settings.fetchUserPreferences()
        // resolve promise
        resolve({username: 'Dio', logged: true});
      } else {
        alert('error in login user')
        auth.authenticate(false)
        settings.$reset()
        // reject promise
        reject(null);
      }
    });
  })
}

export const useUserStore = defineStore('users', {
  state: (): State => ({
    userData: null
  }),
  actions: {
    async registerUser(username: string, password: string){
      try{
        this.userData = await registerUser(username, password)
        console.log(this.userData)
      }catch(error){
        console.error(error)
        return error
      }
    }
  }
})