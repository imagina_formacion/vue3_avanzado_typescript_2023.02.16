import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  function increment() {
    count.value++
  }

  function randomizeCounter() {
    count.value = Math.round(100 * Math.random())
  }

  return { count, doubleCount, increment, randomizeCounter }
})
