import {defineStore} from 'pinia'

import {useAuthStore} from './auth'

interface Preferences {
  theme: string
}
interface State {
  preferences: null|Preferences
}

/**
 * It returns a promise that resolves to an object with a property called theme that has a value of
 * 'dark'
 * @returns A promise that resolves to an object with a theme property.
 */
const fetchPreferences = (): Promise<Preferences> => {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      resolve({theme:'dark'})
    })
  })
} 

/* Creating a store with the name 'settings' and it is defining the state and actions of the store. */
export const useSettingsStore = defineStore('settigns', {
  state: (): State => ({
    preferences: null
  }),
  actions: {
    async fetchUserPreferences() {
      const auth = useAuthStore()
      if(auth.isAuthenticated){
        ths.preferences = await fetchPreferences()
      } else {
        throw new Error ('user must be authenticated')
      }
    }
  }
})
// 