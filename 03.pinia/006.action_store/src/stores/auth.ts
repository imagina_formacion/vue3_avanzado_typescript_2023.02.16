import {defineStore} from 'pinia'

interface State {
  isAuthenticated: boolean
}

export const useAuthStore = defineStore('auth', {
  state: (): State => ({
    isAuthenticated: false
  }),
  actions: {
    authenticate(auth){
      this.isAuthenticated = auth
    }
  }
}) 