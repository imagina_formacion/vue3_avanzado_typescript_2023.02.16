import { createRouter, createWebHistory } from 'vue-router'
// import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: {name: 'state-pinia'}
    },
    {
      path: '/state-pinia',
      name: 'state-pinia',
      // route level code-splitting
      // this generates a separate chunk (state-pinia.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/StateView.vue')
    }
  ]
})

export default router
