import { defineStore } from "pinia";
// import products from "@/data/products.json";

interface Product {
  id: string,
  image: string,
  inStock: number,
  name: string,
  price: number,
}

interface State {
  products: Product[]
}

export const useProductStore = defineStore("ProductStore", {
  state: (): State => {
    return {
      // products,
      products: [],
    };
  },
  // actions
  actions: {
    /* A way to import a json file. */
    async fill() {
      this.products = (await import("@/data/products.json")).default;
    },
  },
  // getters
});