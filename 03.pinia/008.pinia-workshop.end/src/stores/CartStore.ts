// import { defineStore } from "pinia";
import { defineStore, acceptHMRUpdate } from "pinia";
import { useLocalStorage } from "@vueuse/core";
import type { RemovableRef } from '@vueuse/shared';
import { groupBy } from "lodash";
import { useAuthUserStore } from "@/stores/AuthUserStore";

interface Product {
  id: string,
  image: string,
  inStock: number,
  name: string,
  price: number,
}

interface State {
  items: RemovableRef<Product[]>,
  test: string
}

console.log(useLocalStorage("CartStore:items", []))

export const useCartStore = defineStore("CartStore", {
  historyEnabled: true,
  state: (): State => {
    return {
      // items: [],
      items: useLocalStorage("CartStore:items", []),
      test: "hello world",
    };
  },
  getters: {
    count: (state) => state.items.length,
    isEmpty: (state) => state.count === 0,
    /* Grouping the items by name and then sorting them. */
    grouped: (state) => {
      const grouped = groupBy(state.items, (item: Product) => item.name);
      const sorted = Object.keys(grouped).sort();
      let inOrder: {[key: string]: Product} = {};
      sorted.forEach((key) => (inOrder[key] = grouped[key]));
      // console.log(inOrder)
      return inOrder;
    },
    groupCount: (state) => (name: string) => state.grouped[name].length,
    total: (state) => state.items.reduce((p, c) => p + c.price, 0),
  },
  actions: {
    /* Using the `useAuthUserStore()` to get the username and then alerting the user that they just
    bought `this.count` items at a total of `this.total` */
    checkout() {
      const authUserStore = useAuthUserStore();
      alert(
        `${authUserStore.username} just bought ${this.count} items at a total of $${this.total}`
      );
    },
    /* Adding items to the cart. */
    addItems(count: number, item: Product) {
      // count = parseInt(count);
      for (let index = 0; index < count; index++) {
        this.items.push({ ...item });
      }
    },
    /* Removing the item from the cart. */
    clearItem(itemName: string) {
      this.items = this.items.filter((item) => item.name !== itemName);
    },
    /* Removing the item from the cart and then adding the item to the cart. */
    setItemCount(item: Product, count: number) {
      this.clearItem(item.name);
      this.addItems(count, item);
    },
    reset() {
      this.items = []
      useLocalStorage("CartStore:items", [])
    }
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useCartStore, import.meta.hot));
}