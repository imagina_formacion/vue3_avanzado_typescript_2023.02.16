import { defineStore } from "pinia";

interface State {
  username: string
}
export const useAuthUserStore = defineStore("AuthUserStore", {
  state: (): State => {
    return {
      username: "hector franco",
    };
  },
  actions: {
    /* Opening a new tab in the browser. */
    visitLinkedinProfile() {
      window.open(`https://www.linkedin.com/in/hector-franco-aceituno/`, "_blank");
    },
  },
});