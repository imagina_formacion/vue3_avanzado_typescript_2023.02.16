import { ref, reactive } from "vue";
/**
 * It's a plugin that adds a history and future array to the pinia instance. It also adds two functions
 * to the pinia instance: undo and redo
 * @param  - `pinia` - The pinia instance.
 * @returns The plugin is returning an object with the history, future, undo and redo functions.
 */
export function PiniaHistoryPlugin({ pinia, app, store, options }) {
  if (!options.historyEnabled) return;
  const history = reactive([]);
  const future = reactive([]);
  const doingHistory = ref(false);
  history.push(JSON.stringify(store.$state));

  store.$subscribe((mutation, state) => {
    /* Checking if the history is being done. If it is not, it is pushing the state to the history
    array and clearing the future array. */
    if (!doingHistory.value) {
      history.push(JSON.stringify(state));
      future.splice(0, future.length);
    }
  });
  return {
    history,
    future,
    /* A function that is undoing the last action. */
    undo: () => {
      if (history.length === 1) return;
      doingHistory.value = true;
      future.push(history.pop());
      store.$state = JSON.parse(history.at(-1));
      doingHistory.value = false;
    },
    /* Taking the latest state from the future array and pushing it to the history array. */
    redo: () => {
      const latestState = future.pop();
      if (!latestState) return;
      doingHistory.value = true;
      history.push(latestState);
      store.$state = JSON.parse(latestState);
      doingHistory.value = false;
    },
  };
}