import { createApp } from "vue";

import { createPinia } from "pinia";
import { PiniaHistoryPlugin } from "@/plugins/PiniaHistoryPlugin";
const pinia = createPinia();
pinia.use(PiniaHistoryPlugin);

import App from "./App.vue";
import BoilerplatePlugin from "./plugins/BoilerplatePlugin";

// Init App
createApp(App)
  // .use(createPinia())
  .use(pinia)
  .use(BoilerplatePlugin)
  .mount("#app");
