import { defineStore } from 'pinia'

export const useOtherStore = defineStore('other', {
  state: () => ({
    data: 'arriquitaum'
  }),
  getters: {
  },
})
