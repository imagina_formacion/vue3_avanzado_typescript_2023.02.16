import { defineStore } from 'pinia'
import {useOtherStore} from '@/stores/other'

export const useCounterStore = defineStore('counter', {
  state: () => ({
    count: 0,
    users: [
      {
        id: 1,
        name: 'manolo',
        active: true
      },
      {
        id: 2,
        name: 'javier',
        active: true
      }
    ],
    localData: 'lorem ipsum'
  }),
  getters: {
    // type is automatically inferred because we are not using `this`
    doubleCount: (state) => state.count * 2,
    // here we need to add the type ourselves (using JSDoc in JS). We can also
    // use this to document the getter
    /**
     * Returns the count value times two plus one.
     *
     * @returns {number}
     */
    doubleCountPlusOne(): number{
      // autocompletion ✨
      return this.doubleCount + 1
    },
    //
    getUserById: (state) => {
      return (userId: number) => state.users.find((user) => user.id === userId)
    },
    getActiveUserById(state) {
      const activeUsers = state.users.filter((user) => user.active)
      return (userId: number) => activeUsers.find((user) => user.id === userId)
    },
    otherGetter(state) {
      const otherStore = useOtherStore()
      return state.localData + ' ' + otherStore.data
    },
  },
})
