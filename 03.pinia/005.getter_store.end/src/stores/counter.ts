import {ref, computed} from 'vue'

import { defineStore } from 'pinia'
import {useOtherStore} from '@/stores/other'

interface UserInfo {
  id: number,
  name: string,
  active: boolean
}
export const useCounterStore = defineStore('counter', () => {
  const count = ref<number>(0)
  const users = ref<UserInfo[]>([
    {
      id: 1,
      name: 'manolo',
      active: true
    },
    {
      id: 2,
      name: 'javier',
      active: true
    }
  ])
  const localData = ref<string>('lorem ipsum')

    // type is automatically inferred because we are not using `this`
  const doubleCount = computed(() => {
    return count * 2
  })
  // here we need to add the type ourselves (using JSDoc in JS). We can also
  // use this to document the getter
  /**
   * Returns the count value times two plus one.
   *
   * @returns {number}
   */
  const doubleCountPlusOne = computed(() => {
    // autocompletion ✨
    return doubleCount + 1
  })
  //
  const getUserById = computed(() => {
    return (userId: number) => users.value.find((user) => user.id === userId)
  })
  const getActiveUserById = computed(() => {
    const activeUsers = users.filter((user) => user.active)
    return (userId: number) => activeUsers.find((user) => user.id === userId)
  })
  const otherGetter = computed(() => {
    const otherStore = useOtherStore()
    return localData.value + ' ' + otherStore.data
  })
  // return
  return { 
    // states
    count, 
    users, 
    localData,
    // getters
    doubleCount,
    doubleCountPlusOne,
    getUserById,
    getActiveUserById,
    otherGetter
  }
})
