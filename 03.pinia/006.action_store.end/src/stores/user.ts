import { defineStore } from 'pinia'

interface User {
  username: string,
  logged: boolean
}

interface State {
  userData: null|User
}

/**
 * RegisterUser is a function that takes a username and password and returns a promise that resolves to
 * a user object or null.
 * @param {string} username - string - The username of the user
 * @param {string} password - string
 * @returns A promise that resolves to a user object or null.
 */
const registerUser = (username: string, password: string): Promise<User|null> => {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      /*stuff using username, password*/
      if ( username == 'Dio' && password == 'irejectmyhumanityjojo') {
        resolve({username: 'Dio', logged: true});
      } else {
        reject(null);
      }
    });
  });
}


export const useUserStore = defineStore('users', {
  state: (): State  => ({
    userData: null,
  }),
  actions: {
    /**
     * The function takes in a username and password, and then calls the registerUser function from the
     * auth.js file. If the function is successful, it will return the userData object. If it fails, it
     * will return an error
     * @param {string} username - string, password: string
     * @param {string} password - string
     * @returns The userData object
     */
    async registerUser(username: string, password: string) {
      try {
        this.userData  = await registerUser(username, password)
        console.log(this.userData)
      } catch (error) {
        console.error(error)
        // let the form component display the error
        return error
      }
    },
  },
})