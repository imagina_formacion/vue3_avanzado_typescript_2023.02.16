import { defineStore } from 'pinia'

import { useAuthStore } from './auth-store'

interface Preferences {
  theme: string
}
interface State {
  preferences: null|Preferences
}

/**
 * FetchPreferences returns a Promise that resolves to an object with a theme property.
 * @returns A promise that resolves to an object with a theme property.
 */
const fetchPreferences = (): Promise<{theme: string}> => {
  return new Promise(function(resolve, reject) {
    setTimeout(() => {
      /*stuff using username, password*/
      resolve({theme:'dark'});
    });
  });
}

export const useSettingsStore = defineStore('settings', {
  state: (): State => ({
    preferences: null,
    // ...
  }),
  actions: {
    async fetchUserPreferences() {
      const auth = useAuthStore()
      if (auth.isAuthenticated) {
        this.preferences = await fetchPreferences()
      } else {
        throw new Error('User must be authenticated')
      }
    },
  },
})