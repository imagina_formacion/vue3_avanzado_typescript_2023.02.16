import { defineStore } from 'pinia'

interface State {
  isAuthenticated: boolean
}

export const useAuthStore = defineStore('users', {
  state: (): State  => ({
    isAuthenticated: true,
  }),
  actions: {  },
})