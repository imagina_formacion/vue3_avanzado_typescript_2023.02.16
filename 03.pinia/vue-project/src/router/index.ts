import { createRouter, createWebHistory } from 'vue-router'
import CounterCompositionView from '@/views/CounterCompositionView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/counter-composition',
      name: 'CounterCompositionView',
      component: CounterCompositionView
    }
    /*
    {
      path: '/',
      name: 'home',
      component: HomeView
    }
    */
  ]
})

export default router
