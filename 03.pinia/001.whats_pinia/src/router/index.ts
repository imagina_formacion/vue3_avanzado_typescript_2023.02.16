import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: { name: 'counter-composition' }
    },
    {
      path: '/counter-composition',
      name: 'counter-composition',
      component: () => import('../views/CounterCompositionView.vue')
    },
    {
      path: '/counter-option',
      name: 'counter-option',
      component: () => import('../views/CounterOptionView.vue')
    }
  ]
})

export default router