import { createRouter, createWebHistory } from 'vue-router'
import StateView from '../views/StateView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'StateView',
      component: StateView
    }
  ]
})

export default router
