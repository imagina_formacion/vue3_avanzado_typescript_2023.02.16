import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

interface UserInfo {
  name: string,
  age: number
}

export const useUserStore = defineStore('user', () => {
  const userList = ref<UserInfo[]>([])
  const user = ref<UserInfo>(null)
  const count = ref<number>(0)
  const hasChanged = ref<boolean>(false)

  function $reset() {
    count.value = 0
  }

  return { 
    // states
    userList, 
    user, 
    count, 
    hasChanged, 
    // actions
    $reset 
  }
})
