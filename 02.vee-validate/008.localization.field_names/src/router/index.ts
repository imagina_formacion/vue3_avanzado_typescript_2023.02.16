import { createRouter, createWebHistory } from 'vue-router'
import Form01 from '@/views/01.init/Form.vue'
import Form02 from '@/views/02.multiple_languages/Form.vue'
import Form03 from '@/views/03.libraries/Form.vue'
import Form04 from '@/views/04.translate_field_names/Form.vue'
import Form05 from '@/views/05.custom_error_messages/Form.vue'



const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: '01',
      component: Form01
    },
    {
      path: '/02',
      name: '02',
      component: Form02
    },
    {
      path: '/03',
      name: '03',
      component: Form03
    },
    {
      path: '/04',
      name: '04',
      component: Form04
    },
    {
      path: '/05',
      name: '05',
      component: Form05
    }
  ]
})

export default router
