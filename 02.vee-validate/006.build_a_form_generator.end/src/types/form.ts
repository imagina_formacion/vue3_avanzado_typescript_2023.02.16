// src/types/form.ts

interface Field {
  label: string,
  name: string,
  as: string,
  type?: string
}

interface FieldDefault {
  type: ' text'
}

export interface Schema {
  fields: FieldDefault []
}

export interface Props {
  schema: Schema
}