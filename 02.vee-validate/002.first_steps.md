## 💁 Usando `useField`

`useField` es una función de la API de [vee-validate](https://vee-validate.logaretm.com/v4/) que nos permite crear modelos de datos que se validan automáticamente y que luego podemos utilizar para construir nuestros propios componentes de entrada personalizados con capacidades de validación. 

Es muy útil si planeas construir una librería de componentes UI que necesite tener capacidades de validación. En otras palabras, actúa como una primitiva que te permite componer lógica de validación en nuestros componentes.

El uso más básico es el siguiente:

```html
<template>
  <div>
    <input v-model="value" />
    <span>{{ errorMessage }}</span>
  </div>
</template>
<script setup>
import { useField } from 'vee-validate';
// a simple `name` field with basic required validator
const { value, errorMessage } = useField('name', inputValue => !!inputValue);
</script>
```

Cada vez que se actualice la referencia `value` se validará, y la referencia `errorMessage` se rellenará automáticamente con el primer mensaje de error. Normalmente enlazaríamos la referencia `value` a nuestras entradas usando `v-model` o cualquier otro medio, y siempre que queramos validar nuestro campo actualizaríamos el enlace `value` con el nuevo valor.

Adicionalmente podríamos usar `yup` como validador:

```html
<script setup>
import { useField } from 'vee-validate';
import * as yup from 'yup';
const { value, errorMessage } = useField('email', yup.string().email().required());
</script>
```

> 👀 **OJO** [vee-validate](https://vee-validate.logaretm.com/v4/) también tra sus propios validadores.

Somos responsables de cuando el campo se valida, se desdibuja o cuando cambia su valor. Esto nos da un mayor control sobre el componente `Field` que puede incluir o implementar valores por defecto sensibles para los casos de uso más comunes.

### ⌨️ Uso con TypeScript

Podemos utilizar `useField` con **typescript** y escribir el tipo de valor del campo para garantizar la seguridad al manipular su valor. La función `useField` es una función genérica que recibe el tipo de valor y lo aplica en las distintas interacciones que tengas con su API.

```ts
const { value, resetField } = useField<string>('email', yup.string().email());
value.value = 1; // ⛔️  Error
value.value = 'test@example.com'; // ✅
resetField({
  value: 1, // ⛔️  Error
});
resetField({
  value: 'test@example.com', // ✅
});
```

Las reglas de validación pueden ser una cadena, un objeto, una función o un esquema yup:

```ts
import { defineRule } from 'vee-validate';

defineRule('required', value => {
  if (!value || !value.length) {
    return 'This field is required';
  }
  return true;
});
```

```ts
// Globally defined rules with `defineRule`, Laravel-like syntax
useField('password', 'required|min:8');
// Globally defined rules object
useField('password', { required: true, min: 8 });
// Simple validation function
useField('password', value => {
  if (!value) {
    return 'password is required';
  }
  if (value.length < 8) {
    return 'password must be at least 8 characters long';
  }
  return true;
});
// Yup validation
useField('password', yup.string().required().min(8));
```

## 🧪 Iniciando nuestra demo

En esta lección, vamos a empezar con [vee-validate](https://vee-validate.logaretm.com/v4/) y validar nuestros primeros campos del formulario.
 
> Si no lo has hecho ya, por favor, hazte con una copia del boilerplate del repositorio.
 
He preparado un ejemplo de formulario de acceso que incluye un campo de correo electrónico y una contraseña. Los detalles del funcionamiento de los componentes no son relevantes para este curso, sólo tiene que ser capaz de emitir su estado a su padre. Además debe ser capaz de mostrar `label` y mensajes de error (`errorMessages`).
 
Con eso fuera del camino, vamos a prepararnos añadiendo el [vee-validate](https://vee-validate.logaretm.com/v4/) en nuestro proyecto.
 
Primero ejecuta el siguiente comando, `npm i -D vee-validate` en tu terminal.

```bash
$ npm i -D vee-validate
```
 
Podemos utilizar [Vee-Validate](https://vee-validate.logaretm.com/v4/) en Vue 3 de dos maneras. A través de un componente basado en una plantilla que nos proporciona la librería llamada `Field` o mediante el uso de la **Composition API**.
 
El enfoque por componentes es la forma más sencilla de utilizar [Vee-Validate](https://vee-validate.logaretm.com/v4/) pero requiere que utilizar su componente de entrada pre-empaquetado. Como queremos aprovechar nuestros componentes personalizados y tener tanto control sobre nuestro formulario como podamos, vamos a utilizar el enfoque de la **Composition API**.

Primero crearemos una nueva vista [src/views/LoginForm.vue](src/views/LoginForm.vue), para empezar a trabajar en la validación de nuestra entrada de correo electrónico.

```html
<!-- src/views/LoginForm.vue -->
<script setup lang="ts">
// components
import BaseButton from "@/components/BaseButton.vue";
import BaseInput from "@/components/BaseInput.vue";
//
const onSubmit = function onSubmit() {
  alert('Submitted')
}
</script>

<template>
  <form @submit.prevent="onSubmit">
    <BaseInput label="Email" type="email"/>
    <BaseInput label="Password" type="password" />

    <BaseButton type="submit" class="-fill-gradient">
      Submit
    </BaseButton>
  </form>
</template>
```

Observaremos que ya tenemos un método de configuración donde estamos declarando un método `onSubmit` para que el evento `@submit` de nuestro formulario tenga algo ligado a él.

Ahora editamos [src/App.vue](src/App.vue) para incluir nuestra vista del formulario:

```diff
<!-- src/App.vue -->
<script setup lang="ts">
// import SimpleForm from '@/views/SimpleForm.vue'
// import ComponentsForm from '@/views/ComponentsForm.vue'
import LoginForm from '@/views/LoginForm.vue'
</script>

<template>
-- <div id="app">
--   <SimpleForm />
--   <hr>
--   <ComponentsForm />
-- </div>
++ <div id="app">
++   <LoginForm />
++ </div>
</template>

<style scoped>
</style>
```

🔖 Empecemos importando la función de composición `useField` de [vee-validate](https://vee-validate.logaretm.com/v4/). 

> La función `useField` le dice a [vee-validate](https://vee-validate.logaretm.com/v4/) que estamos creando un campo de formulario que queremos validar.
 
En su forma más simple acepta dos parámetros. 

* El primero, una cadena con el nombre del modelo para este campo, en nuestro caso, simplemente lo llamaremos `email`.
* El segundo parámetro es una función para comprobar si el valor del campo es válido o no, vamos a utilizar esta función para configurar la validación para nuestras entradas de correo electrónico.

```js
useField('email', , function (value) { /* VALIDATION*/ });
```

Al tratarse de un `email` lo tipificaremos así:

```ts
useField<string>('email', , function (value) { /* VALIDATION*/ });
```
 
Crearemos una constante `email` y llamaremos a la función `useField`.

```diff
<!-- src/views/LoginForm.vue -->
<script setup lang="ts">
// components
import BaseButton from "@/components/BaseButton.vue";
import BaseInput from "@/components/BaseInput.vue";
//
const onSubmit = function onSubmit() {
  alert('Submitted')
}
++ const email = useField<string>('email', , function (value) { /* VALIDATION*/ });
</script>

<template>
  <form @submit.prevent="onSubmit">
    <BaseInput label="Email" type="email"/>
    <BaseInput label="Password" type="password" />

    <BaseButton type="submit" class="-fill-gradient">
      Submit
    </BaseButton>
  </form>
</template>
```
 
> El primer parámetro será una cadena `email` y el segundo parámetro, una función que recibe un valor. Comprobaremos si no hay valor, en ese caso, devolveremos `"This field is required"`, si no, devolveremos `true`.

```diff
<!-- src/views/LoginForm.vue -->
<script setup lang="ts">
// components
import BaseButton from "@/components/BaseButton.vue";
import BaseInput from "@/components/BaseInput.vue";
//
const onSubmit = function onSubmit() {
  alert('Submitted')
}
-- const email = useField<string>('email', , function (value) { /* VALIDATION*/ });
++ const email = useField<string>('email', , function (value) {
++   // input is required validation
++   if (!value) return "This field is required"   
++   return true;
++ });
</script>

<template>
  <form @submit.prevent="onSubmit">
    <BaseInput label="Email" type="email"/>
    <BaseInput label="Password" type="password" />

    <BaseButton type="submit" class="-fill-gradient">
      Submit
    </BaseButton>
  </form>
</template>
```
 
Hemos establecido el nombre `email` para nuestro modelo de esta entrada, y hemos creado una nueva función que validará la entrada de correo electrónico cada vez que se actualice. Observaremos que el formulario recibe un parámetro `value`, `function (value)`. A continuación comprobamos si este valor está vacío (`if (!value)`), si lo está devolvemos una cadena con un mensaje de error (`"This field is required"`).
 
Las funciones de validación en [vee-validate](https://vee-validate.logaretm.com/v4/) pueden devolver:
* `true`, para indicar que el campo es válido
* un `string` que representará el mensaje de error que mostraremos al usuario.
 
Llevemos esto un poco más lejos y utilicemos una expresión regular para comprobar que el valor es una dirección de correo electrónico válida.

```diff
<!-- src/views/LoginForm.vue -->
<script setup lang="ts">
// components
import BaseButton from "@/components/BaseButton.vue";
import BaseInput from "@/components/BaseInput.vue";
//
const onSubmit = function onSubmit() {
  alert('Submitted')
}
const email = useField<string>('email', , function (value) {
  // input is required validation
  if (!value) return "This field is required"
++ // is email
++ const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
++ if (!regex.test(String(value).toLowerCase())) {
++   return 'Please enter a valid email address'
++ }  
  return true;
});
</script>

<template>
  <form @submit.prevent="onSubmit">
    <BaseInput label="Email" type="email"/>
    <BaseInput label="Password" type="password" />

    <BaseButton type="submit" class="-fill-gradient">
      Submit
    </BaseButton>
  </form>
</template>
```

> Aquí, estamos comprobando si el valor que estamos recibiendo del campo email pasa la prueba de expresión regular. Si no lo hace devolvemos una cadena diferente con un error que lo aclare, `'Please enter a valid email address'`.
 
Esto nos da la flexibilidad de hacer todo tipo de comprobaciones y validación aquí para nuestros campos y devolver un mensaje de error diferente para cada uno.
 
Observemos que actualmente estamos guardando el resultado del método `useField` en una constante `email`.

```ts
const email = useField<string>('email', , function (value) {
  // input is required validation
  if (!value) return "This field is required"
  // is email
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (!regex.test(String(value).toLowerCase())) {
    return 'Please enter a valid email address'
  }  
  return true;
});
```
 
Esta constante 📬 `email` es un objeto que contiene dos propiedades muy importantes: 

* El primer `value` es un valor Vue `ref` o `reactive`. Utilizaremos este valor para mantener el enlace de datos entre nuestro componente de entrada y el estado. 
  * Usaremos esta propiedad `value` del objeto `email` para `v-model` en nuestro template. 
  * Primero devolveremos la propiedad `email.value` de nuestra función de configuración. 
  * Estamos exportando esta propiedad de `value` como `emailData` para que quede claro en nuestro `template`.
* La segunda propiedad importante que nos da la función `useField`, es la propiedad `errorMessage`. Siempre que nuestra función de validación falle devolverá un `errorMessage`, será almacenado en esta propiedad de la constante `email`. 
  * Vamos a devolver esta propiedad también en nuestra función de configuración y la vinculamos a la propiedad error de nuestro componente base de entrada.

```diff
<!-- src/views/LoginForm.vue -->
<script setup lang="ts">
// components
import BaseButton from "@/components/BaseButton.vue";
import BaseInput from "@/components/BaseInput.vue";
//
const onSubmit = function onSubmit() {
  alert('Submitted')
}
const email = useField<string>('email', , function (value) {
  // input is required validation
  if (!value) return "This field is required"
  // is email
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (!regex.test(String(value).toLowerCase())) {
    return 'Please enter a valid email address'
  }  
  return true;
});

++ const emailData = email.value
++ const errorMessage = email.errorMessage
</script>

<template>
  <form @submit.prevent="onSubmit">
--  <BaseInput label="Email" type="email"/>
++  <BaseInput label="Email" type="email" v-model="emailData" :error="errorMessage"/>
    <BaseInput label="Password" type="password" />

    <BaseButton type="submit" class="-fill-gradient">
      Submit
    </BaseButton>
  </form>
</template>
```

> _Si ahora vamos a nuestro navegador y comprobamos esto_ 🥰_, veremos que al empezar a escribir la validación se dispara, ya que no estamos introduciendo primero una dirección de correo electrónico válida, la propiedad email está activada y nuestro mensaje de error se muestra._
 
Esto está funcionando muy bien hasta ahora, pero quiero hacer un poco de limpieza 🧹 con la forma en que estamos configurando nuestra constante de correo electrónico y hablar brevemente sobre la desestructuración de JavaScript.
 
Observa 👀 cómo el método `useField` devuelve un objeto, hay bastantes propiedades y las estamos capturando todas como un objeto completo dentro de la constante `email`. 

La **desestructuración** ([Destructuring](https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)) nos permitirá mejorar el código. Eliminaremos la constante `email` y extraeremos `value` y `errorMessage`.

```diff
<!-- src/views/LoginForm.vue -->
<script setup lang="ts">
// components
import BaseButton from "@/components/BaseButton.vue";
import BaseInput from "@/components/BaseInput.vue";
//
const onSubmit = function onSubmit() {
  alert('Submitted')
}
-- const email = useField<string>('email', , function (value) {ç
++ const { value: email, errorMessage: emailError } = useField<string>('email', function (value) {
  // input is required validation
  if (!value) return "This field is required"
  // is email
  const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  if (!regex.test(String(value).toLowerCase())) {
    return 'Please enter a valid email address'
  }  
  return true;
});

-- const emailData = email.value
-- const errorMessage = email.errorMessage
</script>

<template>
  <form @submit.prevent="onSubmit">
--  <BaseInput label="Email" type="email" v-model="emailData" :error="errorMessage"/>
++  <BaseInput label="Email" type="email" v-model="email" :error="emailError"/>
    <BaseInput label="Password" type="password" />

    <BaseButton type="submit" class="-fill-gradient">
      Submit
    </BaseButton>
  </form>
</template>
```
 
El valor de la propiedad y `errorMessage` 😱 del objeto devueltos por `useField` están ahora a nuestra disposición como variables así que podemos pasarlas directamente a la sentencia `return`. 

Hay una última parte que quiero mostrar antes de continuar, hay veces que tendremos que cambiar el nombre de estas propiedades para mayor claridad o porque pueden entrar en conflicto entre sí.
 
En **JavaScript**, podemos renombrar una propiedad desestructurada añadiendo dos puntos después de la propiedad, en la sintaxis desestructurada y el nuevo nombre, primero renombraremos el `value` a `email` y `errorMessage` a `emailError`.
 
```ts
// ...
const { value: email, errorMessage: emailError } = useField<string>('email', function (value) {
```
 
Ahora que hemos validado con éxito nuestros primeros componentes del formulario, estamos listos 🤷 para pasar a la siguiente lección y echar un vistazo a la definición de nuestras validaciones a nivel de formulario, usando un esquema.