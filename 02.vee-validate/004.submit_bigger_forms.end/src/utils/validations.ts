export const required = (value: string): boolean|string => {
  const requiredMessage = 'This field is required'
  if (value === undefined || value === null) return requiredMessage
  if (!String(value).length) return requiredMessage
  return true
}
export const minLength = (number: number, value: string): boolean|string => {
  if (String(value).length < number) return 'Please type at least ' + number + ' characters'
  return true
}
export const anything = (): boolean => {
  return true
}