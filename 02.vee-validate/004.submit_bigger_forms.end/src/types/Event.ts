interface Extras {
  catering: {
    type: boolean,
    default: false,
  },
  music: {
    type: boolean,
    default: false,
  }
}

export default interface Event {
  category: {
    type: String,
    default: '',
  },
  title: {
    type: String,
    default: '',
  },
  description: {
    type: String,
    default: '',
  },
  location: {
    type: String,
    default: '',
  },
  pets: {
    type: Number,
    default: 1,
  },
  extras: Extras
}