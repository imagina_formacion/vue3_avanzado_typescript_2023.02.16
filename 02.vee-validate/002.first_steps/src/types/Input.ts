export interface ErrorMessage {
  id: {
    type: [String, Number],
    required: true
  }
}