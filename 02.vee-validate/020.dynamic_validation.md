[VeeValidate](VeeValidate) viene con 4 modos de interacción fuera de la caja:

* `aggressive`: este es el **comportamiento por defecto** y valida cada vez que se emite un evento de entrada.
* `passive`: no valida en ningún evento, por lo que sólo puedes validar manualmente.
* `lazy`: valida al `change` o `blur` dependiendo del tipo de entrada.
* `eager`: cuando el campo es válido o aún no se ha interactuado con él, se valida al cambiar. Cuando el campo deja de ser válido debido a la primera validación, validará al entrar mientras el campo no sea válido. Cuando el campo vuelva a ser válido, volverá a validar al cambiar. Es una mezcla entre los modos agresivo y perezoso.