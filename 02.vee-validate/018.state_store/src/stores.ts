import { defineStore } from 'pinia';
import { useForm } from 'vee-validate';
import * as Yup from 'yup';

interface Schema {
  name: string,
  email: string,
  password: string
}

/* Defining the validation schema for the form. */
const schema = Yup.object({
  name: Yup.string().required(),
  email: Yup.string().email().required(),
  password: Yup.string().min(6).required(),
});

/* Defining a store. */
export const useSignupStore = defineStore('signup', () => {
  /* Destructuring the useForm hook. */
  const { errors, useFieldModel, handleSubmit } = useForm({
    validationSchema: schema,
  });

  /* Destructuring the useFieldModel hook. */
  const [name, email, password] = useFieldModel(['name', 'email', 'password']);

  /* A function that is called when the form is submitted. */
  const signup = handleSubmit((values: Schema) => {
    // send values to API
    console.log('Submit', JSON.stringify(values, null, 2));
  });

  return {
    errors,
    name,
    email,
    password,
    signup,
  };
});
