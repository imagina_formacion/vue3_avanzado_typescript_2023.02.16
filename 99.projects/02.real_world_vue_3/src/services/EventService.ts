import axios from 'axios'

import db from '@/database/db.json'
/*
const apiClient = axios.create({
  baseURL: 'https://my-json-server.typicode.com/Code-Pop/Real-World_Vue-3',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})
*/

export default {
  getEvents() {
    console.log(db)
    // return apiClient.get('/events')
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({data: db.events});
      }, 300);
    });
  },
  getEvent(id: any) {
    console.log(id)
    // return apiClient.get('/events/' + id)
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({data: db.events.find(event => event.id == id)});
      }, 300);
    });
  }
}
