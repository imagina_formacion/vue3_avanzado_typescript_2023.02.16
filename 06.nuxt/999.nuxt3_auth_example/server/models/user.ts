import type { User } from '~~/types'

// Fake users data
const users: User[] = [
  {
    id: '41dbc5f7-9a4e-42e6-832b-1d3dd8c7c4b6',
    email: 'admin@gmail.com',
    // password: password
    password: '$2a$10$XLEGbbEKPN6WUHyV6Iv9zeT90nZTJl3uz4HPelKblOaQQgEicWijW',
    roles: ['ADMIN'],
  },
  {
    id: 'd0065700-1707-4ad9-811b-8bbed0364318',
    email: 'user@gmail.com',
    // password: password
    password: '$2a$10$XLEGbbEKPN6WUHyV6Iv9zeT90nZTJl3uz4HPelKblOaQQgEicWijW',
    roles: ['USER'],
  },
]

/**
 * It returns a promise that resolves to the users array
 * @returns An array of users
 */
export async function getUsers() {
  return users
}

/**
 * "Get a user by their email address."
 * 
 * The function takes an email address as a parameter and returns a user object
 * @param {string} email - The email of the user you want to get.
 * @returns A promise that resolves to the user object
 */
export async function getUserByEmail(email: string) {
  return users.find(user => user.email === email)
}

/**
 * It returns a user object from the users array, if the user's id matches the id passed in as an
 * argument
 * @param {string} id - string - The id of the user we want to get
 * @returns A promise that resolves to the user object with the matching id.
 */
export async function getUserById(id: string) {
  return users.find(user => user.id === id)
}

/**
 * It returns true if the user is an admin, false otherwise
 * @param {User} [user] - The user object that is passed to the resolver.
 * @returns A function that returns a boolean.
 */
export async function isAdmin(user?: User) {
  return user && user.roles.includes('ADMIN')
}
