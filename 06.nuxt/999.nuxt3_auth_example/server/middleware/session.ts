import { getSession } from '~~/server/utils/session'

export default defineEventHandler(async (event) => {
  /* It's getting the user from the session. */
  const user = await getSession(event)

  /* It's setting the user in the context. */
  if (user)
    event.context.user = user
})
