import type { H3Event } from 'h3'

import cookieSignature from 'cookie-signature'

import { getUserById } from '~~/server/models/user'

/**
 * It takes an object, converts it to a string, encodes it in base64, and returns the result
 * @param {any} obj - The object to serialize.
 * @returns A string
 */
export function serialize(obj: any) {
  const value = Buffer.from(JSON.stringify(obj), 'utf-8').toString('base64')
  const length = Buffer.byteLength(value)

  if (length > 4096)
    throw new Error('Session value is too long')

  return value
}

/**
 * It takes a string, decodes it from base64, and then parses it as JSON
 * @param {string} value - The value to be deserialized.
 * @returns A function that takes a value and returns a JSON object.
 */
export function deserialize(value: string) {
  return JSON.parse(Buffer.from(value, 'base64').toString('utf-8'))
}

/**
 * It takes a string and a secret and returns a signed string
 * @param {string} value - The value to sign.
 * @param {string} secret - The secret used to sign the value.
 * @returns A signed cookie value.
 */
export function sign(value: string, secret: string) {
  return cookieSignature.sign(value, secret)
}

/**
 * It takes a signed cookie value and a secret and returns the original value
 * @param {string} value - The value to unsign.
 * @param {string} secret - The secret used to sign the value.
 * @returns The value of the cookie, if it has been signed. If it has not been signed, or if the
 * signature is invalid, an exception is raised.
 */
export function unsign(value: string, secret: string) {
  return cookieSignature.unsign(value, secret)
}

/**
 * It gets the session cookie from the request, unsigns it, deserializes it, and then gets the user
 * from the database
 * @param {H3Event} event - The event object that is passed to the function.
 * @returns A session object
 */
export async function getSession(event: H3Event) {
  const config = useRuntimeConfig()

  const cookie = getCookie(event, config.cookieName)

  if (!cookie)
    return null

  const unsignedSession = unsign(cookie, config.cookieSecret)

  if (!unsignedSession)
    return null

  const session = deserialize(unsignedSession)

  return getUserById(session.userId)
}
