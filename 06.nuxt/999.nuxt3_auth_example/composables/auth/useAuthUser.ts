import type { UserWithoutPassword } from '~~/types'

/**
 * It returns a tuple of the current user and a function to update the current user
 * @returns A function that returns a tuple of the current value and a function to update the value.
 */
export const useAuthUser = () => {
  return useState<UserWithoutPassword | null>('user', () => null)
}
