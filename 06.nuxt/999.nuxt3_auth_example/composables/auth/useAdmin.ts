export const useAdmin = () => {
  const authUser = useAuthUser()

  /* It's a computed property that returns true if the user is an admin. */
  return computed(() => {
    if (!authUser.value)
      return false

    return authUser.value.roles.includes('ADMIN')
  })
}
