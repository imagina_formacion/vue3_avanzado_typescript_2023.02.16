import { useAuthUser } from './useAuthUser'

export const useAuth = () => {
  const authUser = useAuthUser()

  /**
   * It takes a user object as an argument and sets the authUser.value to that user object
   * @param {any} user - any - this is the user object that is returned from the Firebase
   * authentication API.
   */
  const setUser = (user: any) => {
    authUser.value = user
  }

  /**
   * This function takes a cookie and sets its value to the cookie.
   * @param {any} cookie - The cookie object to set.
   */
  const setCookie = (cookie: any) => {
    cookie.value = cookie
  }

  /**
   * It logs in a user
   * @param {string} email - string,
   * @param {string} password - string,
   * @param {boolean} rememberMe - boolean
   * @returns The authUser object
   */
  const login = async (
    email: string,
    password: string,
    rememberMe: boolean,
  ) => {
    const data = await $fetch('/auth/login', {
      method: 'POST',
      body: {
        email,
        password,
        rememberMe,
      },
    })

    setUser(data.user)

    return authUser
  }

  /**
   * It makes a POST request to the /auth/logout endpoint, and then sets the user to the data.user that
   * comes back from the server
   */
  const logout = async () => {
    const data = await $fetch('/auth/logout', {
      method: 'POST',
    })

    setUser(data.user)
  }

  /**
   * It fetches the user from the server, and if it succeeds, it sets the user in the state
   * @returns The authUser object.
   */
  const me = async () => {
    if (!authUser.value) {
      try {
        const data = await $fetch('/auth/me', {
          headers: useRequestHeaders(['cookie']) as HeadersInit,
        })

        setUser(data.user)
      }
      catch (error) {
        setCookie(null)
      }
    }

    return authUser
  }

  return {
    login,
    logout,
    me,
  }
}
