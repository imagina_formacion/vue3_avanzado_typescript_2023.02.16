# comandos

## `nuxi add`

### `nuxi add component`

```bash
# Generates `components/TheHeader.vue`
npx nuxi add component TheHeader
```

### `nuxi add composable`

```bash
# Generates `composables/foo.ts`
npx nuxi add composable foo
```

### `nuxi add layout`

```bash
# Generates `layouts/custom.vue`
npx nuxi add layout custom
```

### `nuxi add plugin`

```bash
# Generates `plugins/analytics.ts`
npx nuxi add plugin analytics
```

### `nuxi add page`

```bash
# Generates `pages/about.vue`
npx nuxi add page about
```

### `nuxi add middleware`

```bash
# Generates `middleware/auth.ts`
npx nuxi add middleware auth
```

### `nuxi add api`

```bash
# Generates `server/api/hello.ts`
npx nuxi add api hello
```

## `nuxi analyze`

Construye el proyecto nuxt y lo analiza

```{bash}
npx nuxi analyze [rootDir]
``` 

## `nuxi build-module`

## `nuxi build`

## `nuxi cleanup`

```{bash}
npx nuxi clean|cleanup [rootDir]
```

## `nuxi dev`

```{bash}
npx nuxi dev [rootDir] [--dotenv] [--clipboard] [--open, -o] [--no-clear] [--port, -p] [--host, -h] [--https] [--ssl-cert] [--ssl-key]
```

### parámetros

Opción | Por defecto | Descripción
-------------------------|-----------------|------------------
`rootDir` | `.` | El directorio raíz de la aplicación a servir.
`--dotenv` | `.` | Apunta a otro archivo `.env` para cargar, **relativo** al directorio raíz.
`--clipboard` | `false` | Copiar URL al portapapeles.
`--open, -o` | `false` | Abrir URL en el navegador.
`--no-clear` | `false` | No borra la consola tras el arranque.
`--port, -p` | `3000` | Puerto de escucha.
`--host, -h` | `localhost` | Nombre del servidor.
`--https` | `false` | Escuchar con el protocolo `https` con un certificado autofirmado por defecto.
`--ssl-cert` |`null` | Especificar un certificado para https.
`--ssl-key` |`null` | Especifique la clave para el certificado https.

## `nuxi devtools`

```{bash}
npx nuxi devtools enable|disable [rootDir]
```

## `nuxi generate`

```{bash}
npx nuxi generate [rootDir] [--dotenv]
```

## `nuxi info`


```{bash}
npx nuxi info [rootDir]
```

## `nuxi init`

```{bash}
npx nuxi init|create [--verbose|-v] [--template,-t] [dir]
```

## `nuxi prepare`

```{bash}
npx nuxi prepare [rootDir]
```

## `nuxi preview`

```{bash}
npx nuxi preview [rootDir] [--dotenv]
```

## `nuxi typecheck`

```{bash}
npx nuxi typecheck [rootDir]
```

## `nuxi upgrade`

```{bash}
npx nuxi upgrade [--force|-f]
```