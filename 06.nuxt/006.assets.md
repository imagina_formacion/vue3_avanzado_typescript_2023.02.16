## assets

### `public`

```html
<!-- app.vue -->
<template>
  <img src="/img/nuxt.png" alt="Discover Nuxt 3" />
</template>
```

### `assets`


```html
<!-- app.vue -->
<template>
  <img src="~/assets/img/nuxt.png" alt="Discover Nuxt 3" />
</template>
```
**en dev**
```html
<img src="/_nuxt/assets/img/nuxt.png" alt="Discover Nuxt 3">
```
**en prod**
```html
<img src="/_nuxt/nuxt.755b7c95.png" alt="Discover Nuxt 3">
```

### nuxt config

```scss
// assets/_colors.scss
$primary: #49240F;
$secondary: #E4A79D;
```

```scss
// assets/_colors.sass
$primary: #49240F
$secondary: #E4A79D
```

```ts
// nuxt.config
export default defineNuxtConfig({
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "@/assets/_colors.scss" as *;'
        }
      }
    }
  }
})
```

**SASS**
```ts
// nuxt.config
export default defineNuxtConfig({
  vite: {
    css: {
      preprocessorOptions: {
        sass: {
          additionalData: '@use "@/assets/_colors.sass" as *\n'
        }
      }
    }
  }
})
```