## seo y meta

### usando nuxt.config.ts

```ts
// nuxt.config.ts
export default defineNuxtConfig({
  app: {
    head: {
      charset: 'utf-16',
      viewport: 'width=500, initial-scale=1',
      title: 'My App',
      meta: [
        // <meta name="description" content="My amazing site">
        { name: 'description', content: 'My amazing site.' }
      ],
    }
  }
})
```

### usando `useHead`

```html
<!-- app.vue -->
<script setup lang="ts">
useHead({
  title: 'My App',
  meta: [
    { name: 'description', content: 'My amazing site.' }
  ],
  bodyAttrs: {
    class: 'test'
  },
  script: [ { children: 'console.log(\'Hello world\')' } ]
})
</script>
```

### usando `useSeoMeta` y `useServerSeoMeta`

```html
<!-- app.vue -->
<script setup lang="ts">
useServerSeoMeta({
  title: 'My Amazing Site',
  ogTitle: 'My Amazing Site',
  description: 'This is my amazing site, let me tell you all about it.',
  ogDescription: 'This is my amazing site, let me tell you all about it.',
  ogImage: 'https://example.com/image.png',
  twitterCard: 'summary_large_image',
})
</script>
```

### system reactive `useServerSeoMeta`

```html
<!-- app.vue -->
<script setup lang="ts">
const data = useFetch(() => $fetch('/api/example'))
useServerSeoMeta({
  ogTitle: () => `${data.value?.title} - My Site`,
  description: () => data.value?.description,
  ogDescription: () => data.value?.description,
})
</script>
```

## cargar los meta data como componente

```html
<!-- app.vue -->
<script setup>
const title = ref('Hello World')
</script>
<template>
  <div>
    <Head>
      <Title>{{ title }}</Title>
      <Meta name="description" :content="title" />
      <Style type="text/css" children="body { background-color: green; }" />
    </Head>
    <h1>{{ title }}</h1>
  </div>
</template>
```


### `useHead`

**useHead**
```html
<script setup lang="ts">
const desc = ref('My amazing site.')
useHead({
  meta: [
    { name: 'description', content: desc }
  ],
})
</script>
```
**Components**
```html
<script setup>
const desc = ref('My amazing site.')
</script>
<template>
<div>
  <Meta name="description" :content="desc" />
</div>
</template>
```

### ¿Cómo podemos manejar los title?

**useHead**
```html
<script setup lang="ts">
  useHead({
    titleTemplate: (titleChunk) => {
      return titleChunk ? `${titleChunk} - Site Title` : 'Site Title';
    }
  })
</script>
```

### ¿Cómo podemos añadir scripts?


```html
<script setup lang="ts">
useHead({
  script: [
    {
      src: 'https://third-party-script.com',
      body: true
    }
  ]
})
</script>
```

## `definePageMeta`

```html
<!-- pages/some-page.vue -->
<script setup>
definePageMeta({
  title: 'Some Page'
})
</script>
```

```html
<!-- layouts/default.vue -->
<script setup>
const route = useRoute()
useHead({
  meta: [{ property: 'og:title', content: `App Name - ${route.meta.title}` }]
})
</script>
```

### ¿cómo construir un título dinámico?

```html
<!-- app.vue -->
<script setup>
  useHead({
    // as a string,
    // where `%s` is replaced with the title
    // titleTemplate: '%s - Site Title',
    // ... or as a function
    titleTemplate: (productCategory) => {
      return productCategory
        ? `${productCategory} - Site Title`
        : 'Site Title'
    }
  })
</script>
```

### ¿Cómo podemos añadir style sheet?

**useHead**
```html
<script setup lang="ts">
useHead({
  link: [
    {
      rel: 'preconnect',
      href: 'https://fonts.googleapis.com'
    },
    {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css2?family=Roboto&display=swap',
      crossorigin: ''
    }
  ]
})
</script>
```

**Components**
```html
<template>
<div>
  <Link rel="preconnect" href="https://fonts.googleapis.com" />
  <Link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" crossorigin="" />
</div>
</template>
```



