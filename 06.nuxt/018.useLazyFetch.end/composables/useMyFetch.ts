import type { Ref } from 'vue'

type UseFetchOptions = {
  key?: string
  method?: "GET" | "HEAD" | "PATCH" | "POST" | "PUT",
  query?: URLSearchParams
  params?: URLSearchParams
  headers?: HeadersInit | Ref<HeadersInit | undefined> | undefined
  baseURL?: string
  server?: boolean
  lazy?: boolean
}

export const useMyApi = async ( url: string, options: UseFetchOptions ) => await useLazyFetch( url, {
  // your defaults
  baseURL: 'https://pokeapi.co/api/v2',
  key: url,
  ...options
} )