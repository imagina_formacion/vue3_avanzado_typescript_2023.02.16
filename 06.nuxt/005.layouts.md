## layouts

**definir un layout por página**
```html
<!-- pages/index.vue -->
<script>
// This will work in both `<script setup>` and `<script>`
definePageMeta({
  layout: "custom",
});
</script>
```

**layout por defecto**
```html
<!-- layouts/default.vue -->
<template>
  <div>
    Some default layout shared across all pages
    <slot />
  </div>
</template>
```

**¿Cómo lo usamos?**
```html
<!-- app.vue -->
<template>
  <NuxtLayout>
    some page content
  </NuxtLayout>
</template>
```

**definir el layout sobre el componente `NuxtLayout`**
```html
<!-- app.vue -->
<template>
  <NuxtLayout :name="layout">
    <NuxtPage />
  </NuxtLayout>
</template>

<script setup>
// You might choose this based on an API call or logged-in status
const layout = "custom";
</script>
```

**se puede pisar mediante `definePageMeta()`**

**¿cómo cambio el layout?**
```html
<template>
  <div>
    <button @click="enableCustomLayout">Update layout</button>
  </div>
</template>

<script setup>
function enableCustomLayout () {
  setPageLayout('custom')
}
definePageMeta({
  layout: false,
});
</script>
```

**anular un layout**
```html
<!-- pages/index.vue -->
<template>
  <div>
    <NuxtLayout name="custom">
      <template #header> Some header template content. </template>
      The rest of the page
    </NuxtLayout>
  </div>
</template>

<script setup>
definePageMeta({
  layout: false,
});
</script>
```