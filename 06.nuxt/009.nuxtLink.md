
```html
<!-- app.vue -->
<template>
  <NuxtLink to="https://nuxtjs.org">
    Nuxt website
  </NuxtLink>
  
  <!-- <a href="https://nuxtjs.org" rel="noopener noreferrer">...</a> -->
</template>
```

```html
<!-- pages/index.vue -->
<template>
  <NuxtLink to="/about">
    About page
  </NuxtLink>
  <!-- <a href="/about">...</a> (+Vue Router & prefetching) -->
  <NuxtLink :to="{name:'about'}">
    About page (with params)
  </NuxtLink>
</template>
```

### props

* **to** 
* **href**
* **target**, `target="_blank"` 
* **rel**, `noopner`y `noreferrer`
* **noRel**
* **activeClass**
* **exactActiveClass**
* **replace**
* **external**
* **custom** 

**¿cómo pudeo definir un componente?**

```js
// components/MyNuxtLink.ts
export default defineNuxtLink({
  componentName: 'MyNuxtLink',
  /* see signature below for more */
})
```