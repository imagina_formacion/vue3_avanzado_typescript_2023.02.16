import type { Ref } from 'vue'

type UseFetchOptions = {
  key?: string
  method?: "GET" | "HEAD" | "PATCH" | "POST" | "PUT",
  query?: URLSearchParams
  params?: URLSearchParams
  headers?: HeadersInit | Ref<HeadersInit | undefined> | undefined
  baseURL?: string
  server?: boolean
  lazy?: boolean
}

export const useMyApi = async ( url: string, options: UseFetchOptions ) => await useFetch( url, {
  // your defaults
  baseURL: 'https://pokeapi.co/api/v2',
  key: url,
  onRequest () {
    console.log('onRequest')
  },
  onRequestError () {
    console.log('onRequestError')
  },
  async onResponse () {
    console.log('onResponse')
    // Invalida pokemon data en segundo plano si la solicitud tuvo éxito.
    // await refreshNuxtData(url) 
  },
  ...options
})