const styles = ['background: orange', 'color: white', 'display: block'].join(';');
// generate console.info message with styles
function info (...message: string[]) {
  console.info('%c%s', styles, message);
}

export default defineNuxtPlugin(() => {
  const timer = useState('timer', () => 0)

  /* Adding a route middleware that waits 5 seconds before navigating to the next route. */
  if (process.client) {
    addRouteMiddleware(async () => {
      // show info
      info('⌛ Starting timer...')
      timer.value = 5
      do {
        await new Promise(resolve => setTimeout(resolve, 100))
        timer.value--
      } while (timer.value)
      // show info
      info('🧭 ...and navigating')
    })
  }

  /* Preventing the user from accessing the /forbidden route. */
  addRouteMiddleware((to) => {
    if (to.path === '/forbidden') {
      info('❌ to forbidden')
      return false
    }
  })

  /* Adding a route middleware that redirects to /secret if the path is /redirect. */
  addRouteMiddleware((to) => {
    const { $config } = useNuxtApp()
    if ($config) {
      // show info
      info('🔒 Accessed runtime config within middleware.')
    }

    if (to.path !== '/redirect') { return }

    // show info
    info('Heading to', to.path, 'but I think we should go somewhere else...')
    return '/secret'
  })
})
