## `useNuxtData`

`useNuxtData`da acceso al valor en caché de las peticiones `useFecth`, ...

### Mostrar datos antiguos mientras se obtienen en segundo plano

```ts 
// archive.vue
// We can access same data later using 'posts' key
const { data } = await useFetch('/api/posts', { key: 'posts' })
```

```ts 
// single.vue
// Access to the cached value of useFetch in archive.vue
const { data: posts } = useNuxtData('posts')

const { data } = await useFetch(`/api/posts/${postId}`, {
  key: `post-${postId}`,
  default: () => {
    // Find the individual post from the cache and set it as the default value.
    return posts.value.find(post => post.id === postId)
  }
})
```

### Actualizaciones de cache

```ts 
// todos.vue
// We can access same data later using 'todos' key
const { data } = await useFetch('/api/todos', { key: 'todos' })
```

```ts
// add-todo.vue
const newTodo = ref('')
const previousTodos = ref([])

// Access to the cached value of useFetch in todos.vue
const { data: todos } = useNuxtData('todos')

const { data } = await useFetch('/api/addTodo', {
  key: 'addTodo',
  method: 'post',
  body: {
    todo: newTodo.value
  },
  onRequest () {
    previousTodos.value = todos.value // Store the previously cached value to restore if fetch fails.

    todos.value.push(newTodo.value) // Optimistically update the todos.
  },
  onRequestError () {
    todos.value = previousTodos.value // Rollback the data if the request failed.
  },
  async onResponse () {
    await refreshNuxtData('todos') // Invalidate todos in the background if the request succeeded.
  }
})
```