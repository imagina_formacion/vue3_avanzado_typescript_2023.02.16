import { defineCorsEventHandler } from '@nozomuikuta/h3-cors';

export default defineNuxtConfig({
  modules: [
    '@nuxt/ui'
  ],
  devServerHandlers: [
    {
      route: '/',
      handler: defineCorsEventHandler({}),
    },
  ],
  // With vite
  vite: {
    server: {
      headers: {
        'Access-Control-Allow-Origin': '*',
      }
    }
  },
})
